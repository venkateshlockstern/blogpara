@extends('layouts.newlayout')

@section('main')
<style>
    .login{
        margin-top: 5%;
        margin-bottom: 10%
    }
    label{
        margin-left: -14px;
    }
    .rememberme{
        margin-left:30px;
    }
    input#remember {
    margin-left: 0px;
}
</style>
<div class="row login">
    <div class="col-lg-1 col-md-2"></div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-12 offset-lg-2 offset-md-1 blog_toppadder40">
        <div class="blog_contact_form_div wow fadeInUp">
            <div class="blog_main_heading_div">
                <div class="blog_heading_div">
                    <h3 class="blog_bg_lightgreen">{{ __('Reset Password') }}</h3>
                </div>
            </div>
            @if (session('status'))
                   <div class="alert alert-success" role="alert">
                       {{ session('status') }}
                   </div>
               @endif
            <div class="blog_contact_form_div_inner">
                <div class="contact_form">
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="blog_form_group">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                       </div>
                        </div>
                      
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <button id="send_btn"  type="submit" class="blog_btn blog_bg_pink"> {{ __('Send Password Reset Link') }}</button>
                            <div id="err"></div>

                        </div>


                        
                      

                 


                        
                      
                    </div>

                </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row" >

</div>

@endsection
