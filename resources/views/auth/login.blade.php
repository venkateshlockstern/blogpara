@extends('layouts.newlayout')
@include('inc.function')
@section('main')
<style>
    .login{
        margin-top: 5%;
        margin-bottom: 10%
    }
    label{
        margin-left: -14px;
    }
    .rememberme{
        margin-left:30px;
    }
    input#remember {
    margin-left: 0px;
}
</style>
<div class="row login">
    <div class="col-lg-1 col-md-2"></div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-12 offset-lg-2 offset-md-1 blog_toppadder40">
        <div class="blog_contact_form_div wow fadeInUp">
            <div class="blog_main_heading_div">
                <div class="blog_heading_div">
                    <h3 class="blog_bg_lightgreen">Login</h3>
                </div>
            </div>
            <div class="blog_contact_form_div_inner">
                <div class="contact_form">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="blog_form_group">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="someone@example.com">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                       </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="blog_form_group">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="blog_form_group">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label rememberme" for="remember" >
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <button id="send_btn"  type="submit" class="blog_btn blog_bg_pink">Login</button>
                            <div id="err"></div>

                            @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                        </div>


                        
                      

                 


                        
                      
                    </div>

                </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row" >

</div>

@endsection