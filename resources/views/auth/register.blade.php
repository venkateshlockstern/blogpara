@extends('layouts.newlayout')
@include('inc.function')
@section('main')
<style>
    .login{
        margin-bottom: 2%
    }
    label{
        margin-left: -14px;
    }
    .rememberme{
        margin-left:30px;
    }
    input#remember {
    margin-left: 0px;
}
#categorydiv{
    display:none
}
</style>
<div class="row login">
    <div class="col-lg-1 col-md-2"></div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-12 offset-lg-2 offset-md-1 blog_toppadder40">
        <div class="blog_contact_form_div wow fadeInUp">
            <div class="blog_main_heading_div">
                <div class="blog_heading_div">
                    <h3 class="blog_bg_orange">Register</h3>
                </div>
            </div>
            <div class="blog_contact_form_div_inner">
                <div class="contact_form">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="blog_form_group">
                                <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('Type') }}</label>
                                <select id="type"  class="form-control @error('type') is-invalid @enderror" name="type"  required  autofocus>
                                    <option value="user">Individual</option>
                                    <option value="company">Corporate</option>
                                </select>
                                @error('type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12" id="categorydiv">
                            <div class="blog_form_group">
                                <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>
                                <input id="category" type="text" class="form-control @error('category') is-invalid @enderror" name="category" value="{{ old('category') }}"  autocomplete="category" autofocus placeholder="Ex. IT, BPO..">
                                @error('category')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="blog_form_group">
                                <label for="name" class="col-md-4 col-form-label text-md-right" id="namecapture">{{ __('Name') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="blog_form_group">
                                <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="blog_form_group">
                                <label for="email" class="col-md-12 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="someone@example.com">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="blog_form_group">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="blog_form_group">
                                <label for="password-confirm" class="col-md-12 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                      
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <button   type="submit" class="blog_btn blog_bg_pink">{{ __('Register') }}</button>
                            <div id="err"></div>

                        </div>


                        
                      

                 


                        
                      
                    </div>

                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<br>
<br>
<br>
<div class="row mt-5" >

</div>

@endsection
@section('extrascriptss')
    <script>
        $("#type").change(function(){
            var a = $(this).val();
            if (a=="user") {
                $("#categorydiv").fadeOut();

                $("#namecapture").html("Your Name")
            }else{
                $("#categorydiv").fadeIn();
                $("#namecapture").html("Company Name")
            }
        });
    </script>
@endsection