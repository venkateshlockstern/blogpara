@extends('layouts.admindashboard')
@section('posts','active')
@section('extramenu')
    <a href="/create" class="btn btn-outline-primary btn-block mt-3" >
        <span class="fa fa-plus"></span> Create  New Post
    </a>
@endsection
@section('content')
{{-- <div class="row mb-5">
    <div class="col-sm-9"></div>
    <div class="col-sm-3">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#addCat">
            <span class="fa fa-plus"></span> Add Category
        </button>
    </div>
</div> --}}

<div class="table-responsive">
    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th># </th>
                <th>Title</th>
                <th>Username</th>
                <th>Likes</th>
                <th>Comments</th>
                <th>Category</th>
            </tr>
        </thead>
        <tbody>
            @if (count($posts)>0)
            @php $i=1 @endphp
            @foreach ($posts as $post)
            @if ($post->status=='active')
                @php $status='success'    @endphp
            @else
                @php $status='warning'    @endphp
            @endif
        <tr class='clickable-row bg-{{$status}}' data-href='post/{{$post->id}}'>
                    <td>{{$i}}</td>
                    <td>{{$post->title}}</td>
        <td>@php $user=App\User::find($post->user_id) @endphp {{$user->name}} ({{$user->email}})</td>
                    <td>{{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}</td>
                    <td>{{count($comment=App\Comments::where('post_id',$post->id)->get())}}</td>
                    <td>{{$post->category}}</td>
                    @php $i++ @endphp
                </tr>
            @endforeach
            @else

            @endif
        </tbody>

    </table>
</div>


<script>

jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
</script>

@endsection
