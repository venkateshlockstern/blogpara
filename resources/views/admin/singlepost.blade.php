@extends('layouts.admindashboard')
@section('posts','active')
@include('inc.function')
@section('extramenu')
<a href="/edit_post/{{$post->id}}" class="btn btn-outline-info btn-block mt-3">
    <span class="fa fa-edit"></span> Edit Post
</a>
<a href="/approve_post/{{$post->id}}" class="btn btn-outline-warning btn-block mt-3">
    <span class="fa fa-check"></span> Approve / Unapprove
</a>
<a href="/delete_post/{{$post->id}}" class="btn btn-outline-danger btn-block mt-3">
    <span class="fa fa-trash"></span> Delete Post
</a>

@endsection
@section('content')
<div class="card">
    <div class="card-header text-uppercase">{{$post->title }}</div>

    <div class="card-body">
        <div class="clearfix">
            <div class="float-left text-capitalize">
                <b>Category : </b>{{$post->category}}
            </div>
            <div class="float-right" role="alert">
                <i> {{$timeago=get_timeago(strtotime($post->created_at))}} </i>
            </div>
        </div>
        <hr>
        <div class="clearfix">
            <b>Tags : </b>
            @php
            $results= explode(",",$post->tags);
            @endphp
            @foreach ($results as $result)
            # {{$result }}
            @endforeach
        </div>
        <hr>
        <div class="clearfix">
            <div class="float-left">
                <div class="chip mr-0 "><i class="fas fa-heart text-danger" aria-hidden="true"></i> {{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}</div>

            </div>
            <div class="float-right">
                <div class="chip deep-purple  mr-0"><i class="fas fa-comments text-primary"
                    aria-hidden="true"></i> {{count($comment=App\Comments::where('post_id',$post->id)->get())}}</div>
            </div>
        </div>
        <hr>
        <div class="clearfix">
            <div class="float-left">
               <b>Author : </b> @php $user=App\User::find($post->user_id) @endphp {{$user->name}}
            </div>
            <div class="float-right">
               <b>Email : </b> {{$user->email}}
            </div>
        </div>
        <hr>
        <div class="clearfix">
            {!!$post->description!!}
        </div>
        <hr>
        <div class="row">
            <img src="/storage/posts/{!!$post->picture!!}" alt="" class="img-thumbnail img-responsive">
        </div>
        <hr class="mar_tp mar_bt">

        {{-- comments section --}}
        <div class="container mar_bt mar_tp" id="comment_load">
            <h4 class="head4 dashed">Comments </h4>
            <div class="comment-section">
                @if (count($comments)>0)
                @foreach ($comments as $comment)
                @php $user=App\User::findOrFail($comment->user_id); @endphp
                <div class="card">
                    <div class="card-header">
                        <div class="clearfix">
                            <div class="float-left">
                                <b> {{$user->name}}</b>
                            </div>
                            <div class="float-right">
                                <i>{{$timeago=get_timeago(strtotime($comment->created_at))}}</i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">{{$comment->comment}}</div>
                </div>
                @endforeach

                @else
                <div class="alert alert-primary" role="alert">
                    <strong>No Comments Found</strong>
                </div>
                @endif


            </div>

        </div>

    </div>
</div>
@endsection
