@extends('layouts.admindashboard')
@section('users','active')

@section('content')

<div class="card">
    <div class="card-header ">
        <div class="clearfix">
            <div class="float-left text-capitalize">
                <b>Name : </b>{{$user->name}}
            </div>
            <div class="float-right text-capitalize">
                <b>Email : </b> <span class="text-lowercase"> {{$user->email}} </span> 
            </div>
            
        </div>

    </div>
        <div class="card-header">
            <div class="clearfix">
                @if ($user->type=="company")
            <div class="float-left text-capitalize">
                <b>Type : </b>{{$user->type}}
            </div>
            <div class="float-right text-capitalize">
                <b>Category : </b> <span class="text-lowercase"> {{$user->category}} </span> 
            </div>
            @else
            <div class="float-left text-capitalize">
                <b>Type : </b>{{$user->type}}
            </div>
            @endif
            </div>
        </div>
    <div class="card-body">
        <div class="clearfix">
            <div class="float-left text-capitalize">
                <b>Total Posts :</b> {{count($posts)}}
            </div>
            <div class="float-right text-capitalize">
                <b>Total Comments :</b> {{count(App\Comments::where('user_id',$user->id)->get())}}
            </div>
        </div>
        <hr>
        <h4>Posts</h4>

        <div class="table-responsive">
            <table class="table-hover table">
                <thead>
                    <tr>
                        <th># </th>
                        <th>Title</th>
                        <th>Likes</th>
                        <th>Comments</th>
                        <th>Category</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($posts)>0)
                    @php $i=1 @endphp
                    @foreach ($posts as $post)
                    @if ($post->status=='active')
                        @php $status='success'    @endphp
                    @else
                        @php $status='warning'    @endphp
                    @endif
                <tr class='clickable-row bg-{{$status}}' data-href='/post/{{$post->id}}'>
                            <td>{{$i}}</td>
                            <td>{{$post->title}}</td>
                            <td>{{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}</td>
                            <td>{{count($comment=App\Comments::where('post_id',$post->id)->get())}}</td>
                            <td>{{$post->category}}</td>
                            @php $i++ @endphp
                        </tr>
                    @endforeach
                    @else
        
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>






<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });
    });
</script>

@endsection
