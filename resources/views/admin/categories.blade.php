@extends('layouts.admindashboard')
@section('categories','active')
@section('extramenu')
    <button type="button" class="btn btn-outline-primary btn-block mt-3" data-toggle="modal" data-target="#addCat">
        <span class="fa fa-plus"></span> Add Category
    </button>
@endsection

@section('content')
<div class="table-responsive">
    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th># </th>
                <th>Category</th>
                <th>Count</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @if (count($categories)>0)
            @php $i=1 @endphp
            @foreach ($categories as $category)
            @php $posts=App\Posts::where('category',$category->category)->get(); $count=count($posts) @endphp

            <tr>
                <td>{{$i}}</td>
                <td>{{$category->category}}</td>
                <td>{{$count}}</td>
                <td>
                    <a href="/delete_category/{{$category->id}}" class='btn btn-outline-danger '>Delete</a></td>
            </tr>
            @php $i++ @endphp
            @endforeach
            @else
            <tr>
                <td colspan="4" class="text-center">No Record Found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>



<!-- Modal -->
<div class="modal fade" id="addCat" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/admincategories" method="post">
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="">Category</label>
                        <input type="text" name="category" id="" class="form-control" placeholder=""
                            aria-describedby="helpId" required>
                        <small id="helpId" class="text-muted">Will be display in Blog Section</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>

</script>
@endsection
