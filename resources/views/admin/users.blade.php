@extends('layouts.admindashboard')
@section('users','active')

@section('content')

<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Type</td>
                <td>Posts</td>
            </tr>
        </thead>
        <tbody>
            @if (count($users)>0)
            @foreach ($users as $user)
            @if ($user->id!=1)
            <tr class="clickable-row" data-href="/user/{{$user->id}}">
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->type}} @if ($user->type=="company")
                    ( {{$user->category}} )
                @endif</td>
                <td>{{count(App\Posts::where('user_id',$user->id)->get())}}</td>
            </tr>
            @endif
            @endforeach
            @else
            <tr>
                <td colspan="3">No Users Found</td>
            </tr>
            @endif
        </tbody>
    </table>
</div>











<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });
    });
</script>

@endsection
