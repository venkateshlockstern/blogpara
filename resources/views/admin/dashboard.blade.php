@extends('layouts.admindashboard')
@section('home','active')
@section('content')
<style>
h3.card-text {
    color: white;
    border: 5px solid;
    border-radius: 50%;
    /* padding: 40px; */
    padding: 25px 0px;
    font-size: 50px;
}
p.card-text {
    font-size: 20px;
    font-weight: bold;
    /* letter-spacing: 2px; */
    color: white;
    text-transform: uppercase;
    text-decoration: underline dashed;
    padding-bottom: 8px;
}
</style>
<canvas id="myChart" width="100" height="100"></canvas>
<input type="hidden" name="" value="{{count(App\Posts::all())}}" id="post_count">
<input type="hidden" name="" value="{{count(App\User::all())}}" id="user_count">
<input type="hidden" name="" value="{{count(App\Likes::where('like_status',1)->get())}}" id="like_count">
<input type="hidden" name="" value="{{count(App\Comments::all())}}" id="comment_count">

<script>
var post_count=document.getElementById('post_count').value
var user_count=document.getElementById('user_count').value
var like_count=document.getElementById('like_count').value
var comment_count=document.getElementById('comment_count').value
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Posts', 'Users', 'Likes', 'Comments'],
        datasets: [{
            label: 'Overall',
            data: [post_count, user_count, like_count, comment_count],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
});
</script>


{{-- <div class="container">
  <div class="card-group">
    <div class="card bg-primary">
      <div class="card-body text-center">
      <h3 class="card-text"></h3>
        <p class="card-text">Posts</p>
      </div>
    </div>
    <div class="card bg-warning">
      <div class="card-body text-center">
        <h3 class="card-text">{{count(App\User::all())}}</h3>
        <p class="card-text">Users</p>
      </div>
    </div>
    <div class="card bg-success">
      <div class="card-body text-center">
        <h3 class="card-text">{{count(App\Likes::where('like_status',1)->get())}}</h3>
        <p class="card-text">Likes</p>
      </div>
    </div>
    <div class="card bg-danger">
      <div class="card-body text-center">
        <h3 class="card-text">{{count(App\Comments::all())}}</h3>
        <p class="card-text">Comments</p>
      </div>
    </div>
  </div>
</div> --}}
@endsection
