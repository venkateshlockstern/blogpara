@extends('layouts.admindashboard')
@section('posts','active')
@section('content')
<div class="card">
    <div class="card-header text-uppercase">Edit Post</div>
<form action="/update/{{$post->id}}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="card-body">
        <div class="form-group">
            <label for="">Post Title</label>
            <input type="text" name="title" id="" class="form-control" placeholder="" value="{{$post->title}}" aria-describedby="">
        </div>
        <div class="form-group">
            <label for="">Tags</label>
        <input type="text" name="tags" id="" class="form-control" value="{{$post->tags}}" aria-describedby="helpId">
            <small id="helpId" class="text-muted">Separated using ( , ) </small>
        </div>
        <div class="form-group">
            <label for="">Category</label>
            <select class="form-control" name="category" id="">
            <option value="{{$post->category}}">{{$post->category}}</option>
                @if (count($categories)>0)
                @foreach ($categories as $category)
                <option value="{{$category->category}}" class="text-capitalize"> {{$category->category}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label for="">Image</label>
            <input type="file" class="form-control" name="image" id="" placeholder="" accept="image/*"
                aria-describedby="fileHelpId">
            <small id="fileHelpId" class="form-text text-muted">Max 2 MiB</small>
        </div>
        <div class="form-group">
            <label for="">Description</label>
            <textarea class="form-control" id="ckeditor" name="description" id="" rows="3">
                {!!$post->description!!}
            </textarea>
        </div>
        <div class="clearfix">
            <button type="submit" class="btn btn-primary float-right">Update Post</button>
        </div>
    </div>
</form>
</div>

@endsection
