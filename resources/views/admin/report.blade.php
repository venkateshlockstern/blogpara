@extends('layouts.admindashboard')
@section('reports','active')
@section('content')
{{-- <div class="row mb-5">
    <div class="col-sm-9"></div>
    <div class="col-sm-3">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#addCat">
            <span class="fa fa-plus"></span> Add Category
        </button>
    </div>
</div> --}}

<div class="table-responsive">
    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th># </th>
                <th>Name</th>
                <th>Email</th>
                <th>Reason</th>
                <th>Report Description</th>
                <th>Post Title</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @if (count($reports)>0)
            @foreach ($reports as $report)
                <tr>
                <td>{{$report->id}}</td>
                @php $user=App\User::find($report->user_id) ; $post=App\Posts::find($report->post_id);@endphp
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$report->report_statement}}</td>
                <td>{{$report->report_description}}</td>
                <td>{{$post->title}}</td>
                <td><a href="post/{{$post->id}}" class="btn btn-outline-danger">View Post</a></td>
                </tr>
            
            
            @endforeach
            @endif
        </tbody>

    </table>
</div>


<script>

jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
</script>

@endsection
