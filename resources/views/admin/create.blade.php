@extends('layouts.admindashboard')
@section('posts','active')
@section('content')
<div class="card">
    <div class="card-header text-uppercase">Create New Post</div>
<form action="/create" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="card-body">
        <div class="form-group">
            <label for="">Post Title</label>
            <input type="text" name="title" id="" class="form-control" placeholder="" aria-describedby="">
        </div>
        <div class="form-group">
            <label for="">Tags</label>
            <input type="text" name="tags" id="" class="form-control" placeholder="" aria-describedby="helpId">
            <small id="helpId" class="text-muted">Separated using ( , ) </small>
        </div>
        <div class="form-group">
            <label for="">Category</label>
            <select class="form-control" name="category" id="">
                <option>------------------</option>
                @if (count($categories)>0)
                @foreach ($categories as $category)
                <option value="{{$category->category}}" class="text-capitalize"> {{$category->category}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label for="">Image</label>
            <input type="file" class="form-control" name="image" id="" placeholder="" accept="image/*"
                aria-describedby="fileHelpId">
            <small id="fileHelpId" class="form-text text-muted">Max 2 MiB</small>
        </div>
        <div class="form-group">
            <label for="">Description</label>
            <textarea class="form-control" id="ckeditor" name="description" id="" rows="3"></textarea>
        </div>
        <div class="clearfix">
            <button type="submit" class="btn btn-primary float-right">Create Post</button>
        </div>
    </div>
</form>
</div>
<script>
    CKEDITOR.replace('description');
</script>
@endsection
