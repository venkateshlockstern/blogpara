@extends('layouts.admindashboard')
@section('ads','active')

@section('content')

<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#home">Banner ads</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#menu1">Sidebar</a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane container active" id="home">
       
        <form action="/bannerad" method="post">
            @csrf
            <div class="form-group">
                <label for="">Banner Ads</label>
                <textarea class="form-control" name="banner" id="" rows="3"> @php $ad=App\Ads::where('place','banner')->first() @endphp {!!$ad->adcode!!}</textarea>
            </div>
            <input type="submit" value="Change" class="btn btn-danger">
        </form>
    </div>
    <div class="tab-pane container fade" id="menu1">
  
        <form action="/sidebar" method="post">
            @csrf
            <div class="form-group">
                <label for="">Sidebar Ads</label>
            <textarea class="form-control" name="sidebar" id="" rows="3">@php $ad=App\Ads::where('place','sidebar')->first() @endphp {{$ad->adcode}}</textarea>
            </div>
            <input type="submit" value="Change" class="btn btn-primary">
        </form>

    </div>
</div>










<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });
    });
</script>

@endsection
