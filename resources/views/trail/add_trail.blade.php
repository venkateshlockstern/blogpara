@extends('layouts.newlayout')
@include('inc.function')
@section('main')
<style>
    a.badge.badge-danger {
        background: #E91E63;
        padding: 0px 7px;
        color: #fff;
        border-radius: 15px;
    }
.card{
    box-shadow: 0 0 13px 0 rgba(236,236,241,.44);
    background: #fff;
    width: 100%;
    padding: 10px 30px;
    margin-top: 10px;

}
.col-sm-12.view-comment-btn {
    margin-top: -28px;
}
span.bubble {
    background: #E91E63;
    padding: 0px 5px;
    border-radius: 50%;
    color: #fff;
}
.view-comment-btn a {
    color: #007bff;
}
.comment.view-comment-btn{
    margin-top: -28px;
    color: #007bff;
}
.comment-section {
    height: 300px;
    overflow: auto;
}
.post-description {
    background: white;
    padding: 10px 20px;
}
.blog_comment_formdiv h3 {
 margin-bottom: 0px;
}
.card form {
    padding: 50px 20px;
}
</style>
<div class="blog_breadcrumb_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="blog_breadcrumb_div">
                    <h3>Fill Your Para</h3>
                    <ol class="breadcrumb">
                        <li>You are here:</li>
                        <li><a href="/">Home</a></li>
                        <li><a href="#">Blog</a></li>
                        <li class="active">Fill Your Para</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blog_main_wrapper blog_toppadder60 blog_bottompadder60">
    
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                <div class="card">
                  <form action="/add-trail" method="post" enctype="multipart/form-data">
                    @csrf
                       <div class="form-group">
                         <label for="">Title</label>
                         <input type="text" name="title" id="" class="form-control" placeholder="Title" maxlength="90" required aria-describedby="helpId">
                       </div>
                       <input type="hidden" name="thread_status" value="active">
                       <input type="hidden" name="post_type" value="parent_trail">
                       <div class="form-group">
                           <label for="">Category</label>
                           <select name="category" class="form-control" id="" required>
                               <option value="">--------------</option>
                               @php $categories=App\Categories::all(); @endphp
                               @if (count($categories)>0)
                               @foreach ($categories as $category)
                           <option value="{{$category->category}}">{{$category->category}}</option>
                               @endforeach
                               @endif
                           </select>
                       </div>
                       <div class="form-group">
                         <label for="">Tags</label>
                         <input type="text" name="tags" id="" class="form-control" placeholder="" required aria-describedby="helpId">
                         <small id="helpId" class="text-muted">Should be Separated as (,) Comma</small>
                       </div>
                       <div class="form-group">
                         <label for="">Description</label>
                         <textarea type="text"  name="description" class="form-control" required placeholder="" aria-describedby="helpId" maxlength="450"></textarea>
                       </div>
                       
                       <div class="form-group">
                         <label for="">Image</label>
                         <input type="file" name="image" id="" accept="image/*" class="form-control" placeholder="Title" aria-describedby="helpId">
                       </div>
          
          
                       <input type="submit" value="Submit" class="blog_btn blog_bg_blue ">
                </form>

            </div>
            </div>
            @include('inc.theme.sidebar')
        </div>
    </div>
</div>

@endsection