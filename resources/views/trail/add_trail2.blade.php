@extends('layouts.main')
@include('inc.function')
@section('section')
<div class="col-12 col-md-9">
    <form action="/search" method="get">
        <div class="input-group md-form form-sm form-2 pl-0">
            <input class="form-control my-0 py-1 amber-border" type="search" name="search" placeholder="Search"
                aria-label="Search">
            <div class="input-group-append">
                <button class="input-group-text amber lighten-3" id="basic-text1"><i class="fas fa-search text-grey"
                        aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="categ_main mar_tp mar_bt">
        <h4 class="head4 dashed">Add Trail</h4>
       
        <form action="/add-trail" method="post" enctype="multipart/form-data">
          @csrf
             <div class="form-group">
               <label for="">Title</label>
               <input type="text" name="title" id="" class="form-control" placeholder="Title" maxlength="90" required aria-describedby="helpId">
             </div>
             <input type="hidden" name="thread_status" value="active">
             <input type="hidden" name="post_type" value="parent_trail">
             <div class="form-group">
                 <label for="">Category</label>
                 <select name="category" class="form-control" id="" required>
                     <option value="">--------------</option>
                     @php $categories=App\Categories::all(); @endphp
                     @if (count($categories)>0)
                     @foreach ($categories as $category)
                 <option value="{{$category->category}}">{{$category->category}}</option>
                     @endforeach
                     @endif
                 </select>
             </div>
             <div class="form-group">
               <label for="">Tags</label>
               <input type="text" name="tags" id="" class="form-control" placeholder="" required aria-describedby="helpId">
               <small id="helpId" class="text-muted">Should be Separated as (,) Comma</small>
             </div>
             <div class="form-group">
               <label for="">Description</label>
               <textarea type="text"  name="description" class="form-control" required placeholder="" aria-describedby="helpId" maxlength="450"></textarea>
             </div>
             
             <div class="form-group">
               <label for="">Image</label>
               <input type="file" name="image" id="" accept="image/*" class="form-control" placeholder="Title" aria-describedby="helpId">
             </div>


             <input type="submit" value="Submit" class="btn btn-primary ">
      </form>
    </div>






</div>
@endsection