@extends('layouts.main')
@include('inc.function')
@section('section')
<div class="col-12 col-md-9">
    <div class="card">

        <div class="card-body">
            <div class="card-title">
                <h4>{{$post->title}}</h4>
            </div>
            <p>{!!$post->description!!}</p>
        </div>

    </div>
    <div class="card">
        <div class="card-body">
            <div class="panel">
                <div class="panel-head">
                    <h4>Fill your Whisper</h4>
                </div>
                <div class="panel-body">
                    <form action="" method="post">
                        @csrf
                        <textarea type="text" name="description" class="form-control" id=""></textarea>
                        <br>
                        <div class="float-right">
                        <input type="submit" value="Submit" class="btn btn-outline-primary">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection