@extends('layouts.main2')
@include('inc.function')
@php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; @endphp
@section('meta')
      <!-- Facebook and Twitter integration -->
      <meta property="og:title" content="{{$post->title}}" />
      <meta property="og:image" content="http://blogpara.in/storage/posts/{{$post->picture}}" />
      <meta property="og:url" content="{{$actual_link}}" />
      <meta property="og:site_name" content="BlogPara" />
      <meta property="og:description" content="{!!$post->title!!}" />
      <meta name="twitter:title" content="{{$post->title}}" />
      <meta name="twitter:image" content="http://blogpara.in/storage/posts/{{$post->picture}}" />
      <meta name="twitter:url" content="{{$actual_link}}" />
@endsection
@section('section')
<div class="col-12 col-md-9">
    <div class="categ_main mar_tp mar_bt">
   
        <div class="categ_main mar_tp">
            <h4 class="head4 dashed text-capitalize">{{$post->category}}  @php $today=date("Y/m/d") @endphp</h4>
            <!-- Button trigger modal -->
        @if ($post->invitation==null)
            <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#invite">
            Invite
          </button>

         @elseif($post->expiry_date < $today )
         <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#invite">
            Invite change
          </button>
        @endif

            
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="card booking-card">
                        <div class="view overlay">
                            <img src="/storage/posts/{{$post->picture}}" class="img-fluid" alt="Responsive image">
                            <a href="#!">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                        <div class="card-body">
                            <p class="mb-2">{{$timeago=get_timeago(strtotime($post->created_at))}} </p>
                            <h4 class="card-title font-weight-bold"><a>{{$post->title}}</a></h4>
                            @php $postuser=App\User::find($post->user_id); 
                            $parent_post=$post->id; @endphp
        
                            <i class="mt-2 mb-2"> by <a href="/author/{{$postuser->id}}">  {{$postuser->name}}</a></i>
        
                            <p class="card-text">{!! $post->description !!}
                            </p>
                            <div>
                                @php
                                $results= explode(",",$post->tags);
                                @endphp
                                @foreach ($results as $result)
                                  <a href="/filter-by-tags/{{$result}}"><span class="badge badge-light">{{$result}} </span></a>
                                @endforeach
                            </div>
                            
                            @if(count($trails=App\Posts::where('post_type','trail')->where('parent_post',$post->id)->get())>0)
                                @foreach ($trails as $trail)
                                <hr>
                                      <p>  {!!$trail->description!!} </p>
                                      <div class="float-right">
                                        @php $postuser=App\User::find($trail->user_id); @endphp
        
                                <i class="mt-2 mb-2"> by <a href="/author/{{$postuser->id}}">  {{$postuser->name}}</a></i>
        
                                      </div>
                                      <br>
                                @endforeach
                            @else
                                no trails found
                            @endif
                            <hr class="my-4">
                            <ul class="list-unstyled list-inline d-flex justify-content-between mb-0">
                                <li class="list-inline-item mr-0">
                                    <div class="chip mr-0"><i class="fas fa-heart" aria-hidden="true"></i>
                                        {{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}
                                    </div>
                                </li>
                                <li class="list-inline-item mr-0">
                                    <div class="chip deep-purple white-text mr-0"><i class="fas fa-comments"
                                            aria-hidden="true"></i>
                                        {{count($comment=App\Comments::where('post_id',$post->id)->get())}}</div>
                                </li>
    
                            </ul>
                            <div class="text-center">
                                <div class="btn-group">
                                    
                                    <a href="https://facebook.com/sharer.php?display=page&u={{$actual_link}}"  target="_blank" type="button" class="btn "><span class="fab fa-facebook"></span></a>
                                <a href="http://twitter.com/share?text={{$post->title}}&url={{$actual_link}}&hashtags={{$post->tags}}" target="_blank" type="button" class="btn"><span class="fab fa-twitter"></span></a>
                                    <a href="https://www.linkedin.com/sharing/share-offsite/?url={{$actual_link}}" type="button" target="_blank" class="btn"><span class="fab fa-linkedin"></span></a>
                            <a href="https://in.pinterest.com/pin/create/button/?url={{$actual_link}}" type="button"  target="_blank" class="btn"><span class="fab fa-pinterest"></span></a>
                                    <a  href="https://wa.me/?text={{$post->title}} {{ urlencode($actual_link) }}"  type="button" target="_blank" class="btn"><span class="fab fa-whatsapp"></span></a>
                                </div>
                            </div>
    
                        </div>
                    </div>
                </div>
            
    
            </div>
    
    
    
        </div>
    
    
        {{-- comments section --}}
        <div class="container mar_bt mar_tp" id="comment_load">
            <h4 class="head4 dashed">Comments </h4>
            <div class="comment-section">
                @if (count($comments)>0)
                @foreach ($comments as $comment)
                @php $user=App\User::findOrFail($comment->user_id); @endphp
                <div class="card">
                    <div class="card-header">
                        <div class="clearfix">
                            <div class="float-left">
                                <b> {{$user->name}}</b>
                            </div>
                            <div class="float-right">
                                <i>{{$timeago=get_timeago(strtotime($comment->created_at))}}</i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">{{$comment->comment}}</div>
                </div>
                @endforeach
    
                @else
                <div class="alert alert-primary" role="alert">
                    <strong>No Comments Found</strong>
                </div>
                @endif
    
    
            </div>
    
        </div>
    
        <div class="container mar_bt mar_tp">
            @guest
            <div class="alert alert-warning" role="alert">
                <a href="{{ route('login') }}"><strong>Please Login to Comment</strong></a>
            </div>
            @else
    
            <form action="" method="post">
                @csrf
                <input type="hidden" name="post_id" value="{{$post->id}}">
                <div class="form-group">
                    <label for=""></label>
                    <textarea type="text" name="comment" id="" class="form-control" placeholder="" aria-describedby="helpId"
                        style="resize:none"></textarea>
                    <small id="helpId" class="text-muted">Comment as <b> {{Auth::user()->name}} </b> </small>
                </div>
                <input type="submit" value="Comment" class="btn btn-primary float-left">
            </form>
    
            <button class="btn btn-danger float-right likebtn" data-id="{{$post->id}}"><span class="fa fa-thumbs-up"></span>
                <span id="like_status">
                    @php $likes=App\Likes::where('user_id',Auth::user()->id)->where('post_id',$post->id)->get()@endphp
                    @if (count($likes)>0)
                    @foreach ($likes as $like) @endforeach @php
                    if($like->status==0){echo "Like";}else{echo "Liked";}
                    @endphp
                    @else
                    Like
                    @endif
                </span></button>
    
    
            @endguest
    
        </div>
    
        <br>
        <br>
        {{-- related posts --}}
        <div class="container mar_tp mar_bt">
            <h4 class="head4 dashed mar_tp">Related Posts</h4>
            <div class="row">
                @if (count($posts)>0)
                @php $postcount=1 @endphp
                @foreach ($posts as $post)
                <div class="col-12 col-md-6">
                    <div class="card booking-card">
                   
                        <div class="view overlay">
                            <a href="/blog-detail/{{$post->id}}">  <img src="/storage/posts/{{$post->picture}}" class="img-fluid" alt="Responsive image"
                                style="height:240px"></a>
                            <a href="#!">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                        <div class="card-body">
                            <p class="mb-2">{{$timeago=get_timeago(strtotime($post->created_at))}} </p>
                             <h4 class="card-title font-weight-bold"><a href="/blog-detail/{{$post->id}}">{{$post->title}}</a></h4>
                            @php $postuser=App\User::find($post->user_id) @endphp
        
                            <i class="mt-2 mb-2"> by <a href="/author/{{$postuser->id}}">  {{$postuser->name}}</a></i>
        
                            <p class="card-text">{!! substr($post->description,0,250)!!} <a
                                    href="/blog-detail/{{$post->id}}">more</a> </p>
                            <div>
                                @php
                                $results= explode(",",$post->tags);
                                $i=1;
                                @endphp
                                @foreach ($results as $result)
                                @php $i++ @endphp
                                @if ($i<=4)   <a href="/filter-by-tags/{{$result}}"><span class="badge badge-light">{{$result}} </span></a>
                                    @endif
                                    @endforeach
                            </div>
                            <hr class="my-4">
                            <ul class="list-unstyled list-inline d-flex justify-content-between mb-0">
                                <li class="list-inline-item mr-0">
                                    <div class="chip mr-0"><i class="fas fa-heart" aria-hidden="true"></i>
                                        {{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}</div>
                                </li>
                                <li class="list-inline-item mr-0">
                                    <div class="chip deep-purple white-text mr-0"><i class="fas fa-comments"
                                            aria-hidden="true"></i>
                                        {{count($comment=App\Comments::where('post_id',$post->id)->get())}}</div>
                                </li>
        
                            </ul>
                        </div>
                    </div>
                </div>
                @php $postcount++; @endphp
                @if ($postcount==3)
                    @php break; @endphp
                @endif
                @endforeach
                @else
                No Posts Found
                @endif
    
    
            </div>
        </div>


    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="invite" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Invite By Mail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="/invite" method="post">
                <div class="modal-body">
                        @csrf
                    <input type="hidden" name="post_id" value="{{$parent_post}}">
                    <input type="hidden" name="invite_date" value="{{$today=date("Y/m/d")}}">
                    <input type="hidden" name="expiry_date" value="{{$days_ago = date('Y/m/d', strtotime('+5 days', strtotime($today)))}}">
                        <input type="email" placeholder="Please enter your Email here" name="email" class="form-control" id="" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection