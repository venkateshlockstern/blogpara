@extends('layouts.newlayout')
@include('inc.function')
@section('main')
<style>
    a.badge.badge-danger {
        background: #E91E63;
        padding: 0px 7px;
        color: #fff;
        border-radius: 15px;
    }
.card{
    box-shadow: 0 0 13px 0 rgba(236,236,241,.44);
    background: #fff;
    width: 100%;
    padding: 10px 30px;
    margin-top: 10px;

}
.col-sm-12.view-comment-btn {
    margin-top: -28px;
}
span.bubble {
    background: #E91E63;
    padding: 0px 5px;
    border-radius: 50%;
    color: #fff;
}
.view-comment-btn a {
    color: #007bff;
}
.comment.view-comment-btn{
    margin-top: -28px;
    color: #007bff;
}
.comment-section {
    height: 300px;
    overflow: auto;
}
.post-description {
    background: white;
    padding: 10px 20px;
}
.blog_comment_formdiv h3 {
 margin-bottom: 0px;
}
.card form {
    padding: 50px 20px;
}
.invitation {
    padding: 25px;
}
.float-right {
    float: right;
}
</style>
<div class="blog_breadcrumb_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="blog_breadcrumb_div">
                    <h3>Invitation</h3>
                    <ol class="breadcrumb">
                        <li>You are here:</li>
                        <li><a href="/">Home</a></li>
                        <li><a href="#">Profile</a></li>
                        <li class="active">Invitation</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blog_main_wrapper blog_toppadder60 blog_bottompadder60">
    
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                {{--content  --}}
                @if (count($posts)>0)

                @foreach ($posts as $post)
                <div class="card">
                   
                    <div class="invitation">
                        <div class="title">
                            <h4>{{$post->title}}</h4>
                        </div>
                        <p>{!!$post->description!!}</p>
                       @php $user=App\User::findOrFail($post->user_id) @endphp
                      <a href="/author/{{$post->user_id}}" class="author" style="color:#007bff"> {{$user->username }}</a> invited to this whisper
                         <div class="float-right">
                            <a href="/participate/{{$post->id}}" class="blog_btn blog_bg_lightgreen">View invitation</a>
                        </div>
                    </div>
                </div>
                @endforeach
        @else
        
        No Invitation Found
                @endif


                {{-- my contente --}}
            </div>
            @include('inc.theme.sidebar')
        </div>
    </div>
</div>
@endsection