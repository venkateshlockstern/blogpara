@extends('layouts.main')
@include('inc.function')
@section('section')
<div class="col-12 col-md-9">
    <form action="/search" method="get">
        <div class="input-group md-form form-sm form-2 pl-0">
            <input class="form-control my-0 py-1 amber-border" type="search" name="search" placeholder="Search"
                aria-label="Search">
            <div class="input-group-append">
                <button class="input-group-text amber lighten-3" id="basic-text1"><i class="fas fa-search text-grey"
                        aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="categ_main mar_tp mar_bt">
        <h4 class="head4 dashed">Your Trails</h4>
        <a href="/invitation" class="btn btn-danger">View invitation</a>
        <div class="row">
          @if (count($posts)>0)
          @foreach ($posts as $post)
          <div class="col-12 col-md-4">
            <div class="card booking-card">
             
              <div class="view overlay">
                  <a href="/trail_detail/{{$post->id}}">  <img src="storage/posts/{{$post->picture}}" class="img-fluid" alt="Responsive image"
                      style="height:240px"></a>
                  <a href="#!">
                      <div class="mask rgba-white-slight"></div>
                  </a>
              </div>
              <div class="card-body">
                  <p class="mb-2">{{$timeago=get_timeago(strtotime($post->created_at))}} </p>
                   <h4 class="card-title font-weight-bold"><a href="/trail_detail/{{$post->id}}">{{$post->title}}</a></h4>
                  @php $postuser=App\User::find($post->user_id) @endphp

                  <i class="mt-2 mb-2"> by <a href="/author/{{$postuser->id}}">  {{$postuser->name}}</a></i>

                  <p class="card-text">{!! substr($post->description,0,250)!!} <a
                          href="/trail_detail/{{$post->id}}">more</a> </p>
                  <div>
                      @php
                      $results= explode(",",$post->tags);
                      $i=1;
                      @endphp
                      @foreach ($results as $result)
                      @php $i++ @endphp
                      @if ($i<=4)   <a href="/filter-by-tags/{{$result}}"><span class="badge badge-light">{{$result}} </span></a>
                          @endif
                          @endforeach
                  </div>
                  <hr class="my-4">
                  <ul class="list-unstyled list-inline d-flex justify-content-between mb-0">
                      <li class="list-inline-item mr-0">
                          <div class="chip mr-0"><i class="fas fa-heart" aria-hidden="true"></i>
                              {{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}</div>
                      </li>
                      <li class="list-inline-item mr-0">
                          <div class="chip deep-purple white-text mr-0"><i class="fas fa-comments"
                                  aria-hidden="true"></i>
                              {{count($comment=App\Comments::where('post_id',$post->id)->get())}}</div>
                      </li>

                  </ul>
              </div>
          </div>
          </div>

          @endforeach

          @else
          <p class="text-center">No Posts found</p>
          @endif
     

  
        </div>
       </div>






</div>
@endsection