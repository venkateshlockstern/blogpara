@extends('layouts.main')
@include('inc.function')
@section('section')
<div class="col-12 col-md-9">
    <form action="/search" method="get">
        <div class="input-group md-form form-sm form-2 pl-0">
            <input class="form-control my-0 py-1 amber-border" type="search" name="search" placeholder="Search"
                aria-label="Search">
            <div class="input-group-append">
                <button class="input-group-text amber lighten-3" id="basic-text1"><i class="fas fa-search text-grey"
                        aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="categ_main mar_tp mar_bt">
        <h4 class="head4 dashed">Comment Trails</h4>
        <div class="card">
            <div class="card-body">
                <div class="alert alert-info">
                    Parent Post 
                </div>
                <hr>
                <div class="panel">
                <div class="panel-heading">{{$post->title}}</div>
                <hr>
                <div class="panel-body">{!!$post->description!!}</div>
                </div>
                <hr>
                <div class="alert alert-success">
                    Current Trail
                </div>
                <hr>
                <div class="card">
                    <div class="card-body">
                        {!!$trail->description!!}
                    </div>
                </div>
                <hr class="my-4">
                <div class="comment-section">
                    
                    @if (count($comments)>0)
                    @foreach ($comments as $comment)
                    @php $user=App\User::findOrFail($comment->parent_user); @endphp
                    <div class="card">
                        <div class="card-header">
                            <div class="clearfix">
                                <div class="float-left">
                                    <b> {{$user->name}}</b>
                                </div>
                                <div class="float-right">
                                    <i>{{$timeago=get_timeago(strtotime($comment->created_at))}}</i>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">{{$comment->comments}}</div>
                    </div>
                    @endforeach
        
                    @else
                    <div class="alert alert-primary" role="alert">
                        <strong>No Comments Found</strong>
                    </div>
                    @endif
        
        
                </div>
                <hr class="my-4">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post">
                            @csrf
                            <textarea name="comment"   class="form-control" placeholder="Enter your Comment Here......."></textarea>
                                <input type="submit" value="Comment" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>
        </div>
       </div>






</div>
@endsection