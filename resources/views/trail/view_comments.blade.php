@extends('layouts.newlayout')
@include('inc.function')
@section('main')
<style>
    a.badge.badge-danger {
        background: #E91E63;
        padding: 0px 7px;
        color: #fff;
        border-radius: 15px;
    }
.card{
    box-shadow: 0 0 13px 0 rgba(236,236,241,.44);
    background: #fff;
    width: 100%;
    padding: 10px 30px;
    margin-top: 10px;

}
.col-sm-12.view-comment-btn {
    margin-top: -28px;
}
span.bubble {
    background: #E91E63;
    padding: 0px 5px;
    border-radius: 50%;
    color: #fff;
}
.view-comment-btn a {
    color: #007bff;
}
.comment.view-comment-btn{
    margin-top: -28px;
    color: #007bff;
}
.comment-section {
    height: 300px;
    overflow: auto;
}
.post-description {
    background: white;
    padding: 10px 20px;
}
.blog_comment_formdiv h3 {
 margin-bottom: 0px;
}
</style>
<div class="blog_main_wrapper blog_toppadder60 blog_bottompadder60">
    
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12 col-12">

                <div class="blog_technology blog_topheading_slider_nav">
                    <div class="blog_main_heading_div wow fadeInUp">
                        <div class="blog_heading_div">
                            <h3 class="blog_bg_darkblue">{{$post->title}}</h3>
                        </div>
                        
                    </div>
                    <div class="post-description">
                              {!!$post->description!!} 
                    </div>
                </div>
                <br>
                <br>
                <div class="blog_technology blog_topheading_slider_nav">
                    <div class="blog_main_heading_div wow fadeInUp">
                        <div class="blog_heading_div">
                            <h3 class="blog_bg_orange">Current Trail</h3>
                        </div>
                        
                    </div>
                    <div class="post-description">
                              {!!$trail->description!!} 
                    </div>
                </div>

                <br>
                <br>
                     
                <div class="blog_comment_formdiv wow fadeInUp">
                    <h3>Comments <span>({{count($comments)}})</span></h3>
                    <br>
                        <div class="comment-section">
                            
                            @if (count($comments)>0)
                            @foreach ($comments as $comment)
                            @php $user=App\User::findOrFail($comment->parent_user); @endphp
                            <div class="card mt-3">
                                <div class="card-header">
                                    <div class="clearfix">
                                        <div class="">
                                            <b> {{$user->name}}</b>
                                        </div>
                                        <div class="pull-right">
                                            <i>{{$timeago=get_timeago(strtotime($comment->created_at))}}</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">{{$comment->comments}}</div>
                            </div>
                            @endforeach
                
                            @else
                            <div class="alert alert-primary" role="alert">
                                <strong>No Comments Found</strong>
                            </div>
                            @endif
                
                
                        </div>
                </div>
                         
                      
            </div>
            @include('inc.theme.sidebar')
        </div>
    </div>
</div>
@endsection