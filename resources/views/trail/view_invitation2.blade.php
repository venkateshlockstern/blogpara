@extends('layouts.main')
@include('inc.function')
@section('section')
<div class="col-12 col-md-9">
    <form action="/search" method="get">
        <div class="input-group md-form form-sm form-2 pl-0">
            <input class="form-control my-0 py-1 amber-border" type="search" name="search" placeholder="Search"
                aria-label="Search">
            <div class="input-group-append">
                <button class="input-group-text amber lighten-3" id="basic-text1"><i class="fas fa-search text-grey"
                        aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="categ_main mar_tp mar_bt">
        @if (count($posts)>0)

        @foreach ($posts as $post)
        <div class="card">
           
            <div class="card-body">
                <div class="title">
                    <h4>{{$post->title}}</h4>
                </div>
                <p>{!!$post->description!!}</p>
               @php $user=App\User::findOrFail($post->user_id) @endphp
              <a href="/author/{{$post->user_id}}"> {{$user->username }}</a> invited to this whisper
                 <div class="float-right">
                    <a href="/participate/{{$post->id}}" class="btn btn-primary ">View</a>
                </div>
            </div>
        </div>
        @endforeach
@else

No Invitation Found
        @endif
    </div>
</div>
@endsection