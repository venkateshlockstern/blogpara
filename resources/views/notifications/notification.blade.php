
@php $user=Auth::user();
// $count=1;
@endphp
{{-- {{$notifications}} --}}

@if (count($user->unreadNotifications)>0)

@foreach ($user->unreadNotifications as $notification)

    @php
    $datas=$notification->data;
    $postid= $datas['postid'];
    $posttitle= $datas['title'];
    if ($postid=='invitation') {
      
        $link='/invitation';
        $content="New Invite for a Trail";
    }
    else{
        $link='/blog-detail/'.$postid;
        $content="New Post Added : ".$posttitle;
    }
    @endphp
    <li><a href="{{$link}}" > {{$content}} </a></li>

@endforeach
<br>

<li><a href="/marknotificationasread">Mark all as read</a></li>

    
@else
<li>  <a > No new Notification </a> </li>
@endif