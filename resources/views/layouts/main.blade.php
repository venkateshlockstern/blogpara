<html lang="en">

<head>
@include('inc.userhead')
@yield('meta')
</head>

<body>
@include('inc.usernav')

<div class="container">
		<header>
			<div class="row mar_tp">
				@yield('section')
				@include('inc.sidebar')
			</div>
		</header>
	</div>	

 
 @include('inc.footer')
@include('inc.scripts')


</body>

</html>