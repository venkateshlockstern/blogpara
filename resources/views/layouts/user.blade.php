<html lang="en">

<head>
@include('inc.userhead')
@yield('meta')
</head>

<body>
@include('inc.usernav')
 @yield('content')
 @include('inc.footer')
@include('inc.scripts')


</body>
{{-- 
<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Fill your Para</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
         <form action="/create" method="post" enctype="multipart/form-data">
             @csrf
                <div class="form-group">
                  <label for="">Title</label>
                  <input type="text" name="title" id="" class="form-control" placeholder="Title" maxlength="90" required aria-describedby="helpId">
                </div>
                <div class="form-group">
                    <label for="">Category</label>
                    <select name="category" class="form-control" id="" required>
                        <option value="">--------------</option>
                        @if (count($categories)>0)
                        @foreach ($categories as $category)
                    <option value="{{$category->category}}">{{$category->category}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                  <label for="">Tags</label>
                  <input type="text" name="tags" id="" class="form-control" placeholder="" required aria-describedby="helpId">
                  <small id="helpId" class="text-muted">Should be Separated as (,) Comma</small>
                </div>
                <div class="form-group">
                  <label for="">Description</label>
                  <textarea type="text"  name="description" class="form-control" required placeholder="" aria-describedby="helpId" maxlength="450"></textarea>
                </div>
                
                <div class="form-group">
                  <label for="">Image</label>
                  <input type="file" name="image" id="" accept="image/*" class="form-control" placeholder="Title" aria-describedby="helpId">
                </div>

                <input type="submit" value="Submit" class="btn btn-primary  float-right">
         </form>
        </div>
  
      </div>
    </div>
  </div> --}}
  
</html>