<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- Begin Head -->

<head>
@include('inc.theme.head')
</head>
<body>
    <ul class="float-btn">
        <li><a href="/fill-your-para"> Fill your Para</a></li>
      </ul>


    @include('inc.theme.messages')
@include('inc.theme.loader')
@include('inc.theme.nav')
@yield('main')

@include('inc.theme.footer')
</div>
<!--Main js file Style-->
@include('inc.theme.scripts')
<!--Main js file Style-->
</body>
</html>