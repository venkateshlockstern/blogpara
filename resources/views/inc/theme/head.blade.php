<title>Blogpara </title>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta name="keywords" content="">
<meta name="author" content="kamleshyadav">
<meta name="MobileOptimized" content="320">
<!--Start Style -->
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="/js/plugins/swiper/swiper.css">
<link rel="stylesheet" type="text/css" href="/js/plugins/magnific/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="/css/style.css">
<!-- Favicon Link -->
<link rel="shortcut icon" type="image/png" href="/images/favicon.png">
<style>

.blog_topheading_slider_nav nav {
display: none;
}
.blog_logo {
    width: 150px;
}
.blog_main_menu {
    text-align: right;
    padding-right: 50px;
}
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    @yield('meta')