<div class="blog_main_wrapper">
    <div class="blog_main_header">
    <div class="blog_logo">
        <a href="/"><img src="{{asset('userassets/images/logo.png')}}" class="img-fluid" alt="logo"></a>
        <div class="blog_menu_toggle">
            <span>
                <i class="fa fa-bars" aria-hidden="true"></i>
            </span>
        </div>
    </div>
    <div class="blog_main_menu">
        <div class="blog_main_menu_innerdiv">
            {{-- <ul>
                <li class=""><a href="/">Home</a></li>
                @auth
                <li><a href="/fill-your-para">Fill your Para </a></li>
                <li><a href="/add-trail">Start Whisper</a></li>
                @else
                <li><a href="/login">Login </a></li>
                <li><a href="/register">Register</a></li>
                @endauth

                @auth
                <li >
                    @php $user=Auth::user();@endphp
                    @if (count($user->unreadNotifications)>0)
                    <a href="#">Notification ( {{ count($user->unreadNotifications) }} )</a>
                    <ul class="sub-menu">
                        @include('notifications.notification')
                    </ul>
            
                    @else
                    <a href="#">Notification</a>
            
                    @endif
            
             
                </li> 
                


                <li><a href="#">{{ Auth::user()->name }}</a>
                    <ul class="sub-menu">
                        <li><a href="/profile">View Profile</a></li>
                        <li><a href="/my-trails">My Whispers</a></li>
                        <li> <a  href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                         {{ __('Logout') }}
                     </a></li>
   
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                         @csrf
                     </form>
                    </ul>
                </li>


        
                
                @endauth



            </ul> --}}
            <ul>
                <li class=""> <a href="/"> Home</a></li>
                @auth
                <li><a href="/fill-your-para">Write </a> </li>
                <li >
                    @php $user=Auth::user();@endphp
                    @if (count($user->unreadNotifications)>0)
                    <a href="#">Notification <span>  {{ count($user->unreadNotifications) }} </span></a>
                    <ul class="sub-menu">
                        @include('notifications.notification')
                    </ul>
                    @else
                    <a href="#">Notification</a>
                    <ul class="sub-menu">
                        <li><a href="">No New Notification</a></li>
                    </ul>
                    @endif
                </li> 
                <li><a href="#">{{ Auth::user()->name }}</a>
                    <ul class="sub-menu">
                        <li><a href="/profile">View Profile</a></li>
                        <li><a href="/my-trails">My Whispers</a></li>
                        <li> <a  href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                         {{ __('Logout') }}
                     </a></li>
   
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                         @csrf
                     </form>
                    </ul>
                </li>

                @else
                <li><a >Login / Register </a>
                    <ul class="sub-menu">
                        <li><a href="/login">Login </a></li>
                        <li><a href="/register">Register</a></li>
                    </ul>
                </li>
                @endauth
            </ul>
        </div>
    </div>





    <div class="blog_top_search">
        <form class="form-inline"  action="/search" method="get">
            <div class="blog_form_group">
                <input type="search" name="search" class="form-control" placeholder="Search Here...">
            </div>
            <button type="submit" class="blog_header_search" style="cursor:pointer"><svg xmlns="http://www.w3.org/2000/svg" width="16px" height="16px"><path fill-rule="evenodd"  fill="rgb(0, 0, 0)" d="M15.750,14.573 L11.807,10.612 C12.821,9.449 13.376,7.984 13.376,6.459 C13.376,2.898 10.376,-0.000 6.687,-0.000 C2.999,-0.000 -0.002,2.898 -0.002,6.459 C-0.002,10.021 2.999,12.919 6.687,12.919 C8.072,12.919 9.391,12.516 10.520,11.750 L14.493,15.741 C14.659,15.907 14.882,15.999 15.121,15.999 C15.348,15.999 15.563,15.916 15.726,15.764 C16.073,15.442 16.084,14.908 15.750,14.573 ZM6.687,1.685 C9.414,1.685 11.631,3.827 11.631,6.459 C11.631,9.092 9.414,11.234 6.687,11.234 C3.961,11.234 1.743,9.092 1.743,6.459 C1.743,3.827 3.961,1.685 6.687,1.685 Z"/></svg></button>
        </form>
    </div>
</div>