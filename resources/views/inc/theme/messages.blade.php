{{-- success message --}}
@if (session('success'))
<script>swal('Good Job !',"@php echo session('success') @endphp" , 'success');</script>
@endif