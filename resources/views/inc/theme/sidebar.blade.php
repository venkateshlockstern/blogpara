<div class="col-lg-3 col-md-12 col-sm-12 col-12 theiaStickySidebar">
    <div class="blog_sidebar">
        {{-- <div class="widget widget_recent_post wow fadeInUp">
            <div class="blog_main_heading_div">
                <div class="blog_heading_div">
                    <h3 class="blog_bg_orange">Recents</h3>
                </div>
            </div>
            <ul>
                @php $latposts=App\Posts::orderBy('id','desc')->paginate(6) @endphp
                @if (count($latposts)>0)
                    @foreach ($latposts as $latpost)
                        @if ($latpost->post_type=='blog')
                        @php $posttype='blog_detail' @endphp
                        @else
                        @php $posttype='trail_detail' @endphp 
                        @endif
                    <li>
                        <div class="blog_recent_post">
                            <div class="blog_recent_post_img">
                                <img src="/storage/posts/{{$latpost->picture}}"  style="height:50px;width:50px;" class="img-fluid" alt="">
                            </div>
                            <div class="blog_recent_post_content">
                                <h4><a href="/{{$posttype}}/{{$latpost->id}}">{{$latpost->title}}</a></h4>
                                <p>{{$timeago=get_timeago(strtotime($latpost->created_at))}}  <a href="#">-  @if($latpost->post_type=="blog") BLOG @else WHISPER @endif</a></p>
                            </div>
                        </div>
                    </li>
                    @endforeach
                @endif
              
               
            </ul>
        </div> --}}
       
       @auth
        <div class="widget widget_categories wow fadeInUp">
            <div class="blog_main_heading_div">
                <div class="blog_heading_div">
                    <h3 class="blog_bg_orange">Favourite Categories</h3>
                </div>
            </div>
            <ul>
            
                @php $favs=App\Favourites::where('user_id',Auth::user()->id)->get(); @endphp
                @if (count($favs)>0)
                  @foreach ($favs as $fav)
                  <li><a href="/categories/{{$fav->category}}">{{$fav->category}} <span class="fav-top"> <a href="/deletefav/{{$fav->id}}"><span class=" side-del fa fa-trash"></span></a> </span> </a></li>
                  @endforeach
                  <a href="/select_topic">Click here to add new favorite category</a>
               @else
                    No Favourites Found  <a href="/select_topic">Click here to add new favorite category</a>
               @endif
          
         
            </ul>
        </div>
        @endauth
        <div class="widget widget_categories wow fadeInUp">
            <div class="blog_main_heading_div">
                <div class="blog_heading_div">
                    <h3 class="blog_bg_lightgreen">categories</h3>
                </div>
            </div>
            <ul>
        @php $sidecategories=App\Categories::orderBy('id')->paginate(6)@endphp
        @if (count($sidecategories)>0)
            @foreach ($sidecategories as $sidecat)
            <li><a href="/categories/{{$sidecat->category}}">{{$sidecat->category }}  <span>({{count(App\Posts::where('category',$sidecat->category)->get())}})</span></a></li>
            @endforeach
        @endif
            </ul>
        </div>
        <div class="widget widget_newsletter wow fadeInUp">
            <div class="blog_main_heading_div">
                <div class="blog_heading_div">
                    <h3 class="blog_bg_pink">Invite</h3>
                </div>
            </div>
            <div class="blog_newsletter">
                <p>Enter Your Friends Email To Get Notified.</p>
                <form class="form-inline" action="/invite" method="GET">
                    <div class="blog_form_group">
                        <input type="text" class="form-control" name="email" placeholder=" Email Here...">
                    </div>
                    <button type="submit" class="blog_newsletter_btn"><svg xmlns="http://www.w3.org/2000/svg" width="17px" height="16px"><path fill-rule="evenodd"  fill="rgb(255, 255, 255)" d="M16.914,0.038 C16.838,-0.030 16.731,-0.046 16.639,-0.002 L0.147,7.802 C0.058,7.844 0.001,7.934 0.000,8.034 C-0.001,8.134 0.054,8.225 0.142,8.269 L4.810,10.602 C4.895,10.645 4.997,10.635 5.074,10.577 L9.611,7.123 L6.049,10.855 C5.998,10.908 5.972,10.981 5.978,11.055 L6.333,15.760 C6.340,15.864 6.409,15.953 6.507,15.986 C6.533,15.994 6.560,15.999 6.586,15.999 C6.659,15.999 6.729,15.967 6.778,15.909 L9.256,12.986 L12.318,14.476 C12.384,14.508 12.461,14.509 12.529,14.480 C12.597,14.449 12.648,14.391 12.670,14.320 L16.989,0.310 C17.019,0.212 16.990,0.105 16.914,0.038 Z"/></svg></button>
                </form>
            </div>
        </div>
        {{-- <div class="ads_widget wow fadeInUp">
            <a href="#"><img src="https://via.placeholder.com/280x350?text=Ads%20Area" class="img-fluid" alt=""></a>
        </div> --}}
    </div>
</div>