<div class="blog_copyright_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                <p>Copyright &copy; 2020 <a href="/">Blogpara</a>. All Rights Reserved.</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                <ul class="blog_footer_menu">
                    <li><a href="/">Home</a></li>
                    <li><a href="/fill-your-para">Write</a></li>
                    <li><a href="/profile">Profile</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>