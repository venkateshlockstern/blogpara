  <!-- Footer -->
  <footer class="py-5 ">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Membership</a></li>
                    <li><a href="#">RSS Feeds</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <ul>
                    <li><a href="#">Site Help &amp; Feedback</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4">
                <ul>
                    <li><a href="#">Blogs</a></li>
                    <li><a href="#">Downloads</a></li>
                    <li><a href="#">TechRepublic Forums</a></li>
                    <li><a href="#">Meet the Team</a></li>
                </ul>
            </div>
        </div>
        <p class="alg_cen">Copyright © blogpara 2019</p>
    </div>
    <!-- /.container -->
</footer>