<div class="list-group">
    <a href="/" class="list-group-item list-group-item-action @yield('home')">Home</a>
    <a href="/admincategories" class="list-group-item list-group-item-action @yield('categories')">Categories</a>
    <a href="/posts" class="list-group-item list-group-item-action @yield('posts')">Posts</a>
    <a href="/users" class="list-group-item list-group-item-action @yield('users')">Users</a>
    <a href="/ads" class="list-group-item list-group-item-action @yield('ads')">Advertisement</a>
    <a href="/reports" class="list-group-item list-group-item-action @yield('reports')">Reports</a>
  
                    
      <a class="list-group-item list-group-item-action"  href="{{ route('logout') }}" onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      @csrf
  </form>
  
  </div>