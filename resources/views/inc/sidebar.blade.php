@php $ad=App\Ads::where('place','sidebar')->first() @endphp 
<div class="col-12 col-md-3">
    <div class="side_main_ad">
        {!!$ad->adcode!!}
    </div>
    <div class="recom_side mar_tp">
        <h4 class="head4 dashed text-capitalize">Latest Posts  for you </h4>
        <div class="row">
           @php $posts=App\Posts::where('status','active')->where('post_type','blog')->orWhere('post_type','parent_trail')->OrderBy('id','desc')->paginate(6);  @endphp
         
            @foreach ($posts as $post)
            
           @if ($post->post_type=='blog')
           @php $posttype='blog_detail' @endphp
       @else
       @php $posttype='trail_detail' @endphp 
       @endif

            <div class="col-12 col-md-12 mt-2 img-thumbnail">
            <a href="/{{$posttype}}/{{$post->id}}"><img src="/storage/posts/{{$post->picture}}" class="img-fluid" alt="Responsive image">
                <p class="s_para">{!! substr($post->title,0,25)!!}</p></a>
            </div>
            @endforeach
         
        </div>
    </div>
 
    <div class="side_main_ad mar_tp">
        {!!$ad->adcode!!}
    </div>
</div>