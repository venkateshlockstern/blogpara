  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="/"><img src="{{asset('userassets/images/logo.png')}}"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="/" > <i class="fas fa-home"></i> Home</a>
          </li>
          {{-- <li class="nav-item">
            <a class="nav-link" href="#"><i class="fas fa-bell"></i> Notification</a>
          </li> --}}
          

          @guest
          <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          @if (Route::has('register'))
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
              </li>
          @endif
      @else
      <li class="nav-item dropdown">
        @php $user=Auth::user();@endphp
        @if (count($user->unreadNotifications)>0)
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        <i class="fas fa-globe"></i> Notifications <span class="badge badge-pill badge-primary">{{ count($user->unreadNotifications) }}</span>  <span class="caret"></span>
        </a>

        @else
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        <i class="fas fa-globe"></i> Notifications  <span class="caret"></span>
        </a>

        @endif

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
         @include('notifications.notification')
          
        </div>

    </li>

      <li class="nav-item">
        <a href="/fill-your-para" class="nav-link" >Fill Your Para</a>
      </li>
      <li class="nav-item">
        <a href="/add-trail" class="nav-link" >Whisper</a>
      </li>
          <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <i class="fas fa-user-circle"></i> {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a href="/profile" class="dropdown-item"> View Profile</a>
                <a href="/my-trails" class="dropdown-item" >My Whispers</a>

                  <a class="dropdown-item" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
              </div>
          </li>
      @endguest
        </ul>
      </div>
    </div>
  </nav>