
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="">
    <link href="{{asset('https://fonts.googleapis.com/css?family=Open+Sans|Raleway&amp;display=swap')}}"
        rel="stylesheet">
    <script src="{{asset('https://kit.fontawesome.com/4b85a89741.js')}}" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{asset('https://kit-free.fontawesome.com/releases/latest/css/free.min.css')}}"
        media="all">
    <link rel="stylesheet"
        href="{{asset('https://kit-free.fontawesome.com/releases/latest/css/free-v4-font-face.min.css')}}" media="all">
    <link rel="stylesheet"
        href="{{asset('https://kit-free.fontawesome.com/releases/latest/css/free-v4-shims.min.css')}}" media="all">
    <title>Blogpara</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('userassets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('userassets/css/style.css')}}" rel="stylesheet">

    {{-- owl carousel --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" />

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('userassets/images/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('userassets/images/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('userassets/images/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('userassets/images/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('userassets/images/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('userassets/images/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('userassets/images/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('userassets/images/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('userassets/images/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('userassets/images/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('userassets/images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('userassets/images/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('userassets/images/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('userassets/images/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('userassets/images/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">