@extends('layouts.newlayout')
@include('inc.function')
@section('main')
<style>
    a.badge.badge-danger {
        background: #E91E63;
        padding: 0px 7px;
        color: #fff;
        border-radius: 15px;
    }
.card{
    box-shadow: 0 0 13px 0 rgba(236,236,241,.44);
    background: #fff;
    width: 100%;
    padding: 10px 30px;
    margin-top: 10px;

}
.col-sm-12.view-comment-btn {
    margin-top: -28px;
}
span.bubble {
    background: #E91E63;
    padding: 0px 5px;
    border-radius: 50%;
    color: #fff;
}
.view-comment-btn a {
    color: #007bff;
}
.comment.view-comment-btn{
    margin-top: -28px;
    color: #007bff;
}
.comment-section {
    height: 300px;
    overflow: auto;
}
.post-description {
    background: white;
    padding: 10px 20px;
}
.blog_comment_formdiv h3 {
 margin-bottom: 0px;
}
.card form {
    padding: 50px 20px;
}
</style>
<div class="blog_breadcrumb_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="blog_breadcrumb_div">
                    <h3>Fill Your Para</h3>
                    <ol class="breadcrumb">
                        <li>You are here:</li>
                        <li><a href="/">Home</a></li>
                        <li><a href="#">Blog</a></li>
                        <li class="active">Fill Your Para</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blog_main_wrapper blog_toppadder60 blog_bottompadder60">
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                <form  method="post" enctype="multipart/form-data" id="fillpara">
                    @csrf
                       <div class="form-group">
                         <label for="">Title</label>
                            @error('title')
                            <strong class="text-danger">{{ $message }}</strong>
                            @enderror
                         <input type="text" name="title" id="" class="form-control" placeholder="Title" maxlength="60" value="{{ old('title') }}" required aria-describedby="helpId">
                       </div>
                       <input type="hidden" name="thread_status" value="active">
                       <input type="hidden" name="post_type" value="parent_trail">
                       <div class="form-group"> 
                           <label for="">Category</label>
                           @error('category')
                           <strong class="text-danger">{{ $message }}</strong>
                           @enderror
                           <select name="category" class="form-control" id="" required>
                               <option value="">--------------</option>
                               @php $categories=App\Categories::all(); @endphp
                               @if (count($categories)>0)
                               @foreach ($categories as $category)
                           <option value="{{$category->category}}" @if (old('category') == $category->category)
                               selected
                           @endif>{{$category->category}}</option>
                               @endforeach
                               @endif
                           </select>
                       </div>
                       <div class="form-group">
                         <label for="">Tags</label>
                         @error('tags')
                         <strong class="text-danger">{{ $message }}</strong>
                         @enderror
                         <input type="text" name="tags" id="" class="form-control" value="{{ old('tags') }}" placeholder="" required aria-describedby="helpId">
                         <small id="helpId" class="text-muted">Should be Separated as (,) Comma</small>
                       </div>
          
                       <div class="form-group">
                        {{-- @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        
                     @enderror --}}
                   
                         <label for="">Description</label>
                         @error('description')
                         <strong class="text-danger">{{ $message }}</strong>
                         @enderror
                         <small> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; * You can write upto 450 Characters</small>
                        
          
          <br>
          
          
          
          <textarea type="text"  name="description" id="description" onKeyUp='showDialog(this)' class="form-control" required placeholder="" aria-describedby="helpId" maxlength="450">{{ old('description') }}</textarea>
                       </div>
                       
                       <div class="form-group">
                         <label for="">Image</label>
                         <input type="file" name="image" id="" accept="image/*" class="form-control" placeholder="Title" aria-describedby="helpId">
                       </div>
          
                       <br>
                       {{-- <button type="button"  class="blog_btn blog_bg_orange" id="submitinvoke">Submit</button> --}}
                       <div class="row">
                           <div class="col-sm-3">
                            <button id="postasblog" class="blog_btn blog_bg_darkblue">Post as Blog</button>
                        </div>
                        <div class="col-sm-6">
                            <button id="postaswhisper" class="blog_btn blog_bg_pink">Start as Whisper</button>
                        </div>
                       </div>
                      
                </form>

            </div>
            </div>
            {{-- @include('inc.theme.sidebar') --}}
        </div>
    </div>
</div>



@endsection

@section('extrascriptss')
    <script>
        $(document).ready(function(){
            $("#submitinvoke").click(function(){
                $("#confirm-modal").modal("show");
            });
            $("#postasblog").click(function(){
               $("#fillpara").attr('action','/create');
               $("#fillpara").submit();
            });
            $("#postaswhisper").click(function(){
                $("#fillpara").attr('action','/add-trail');
               $("#fillpara").submit();
            });
            var editor = CKEDITOR.instances.description;
            editor.on( 'key', function( evt ){
                var currentLength = editor.getData().length,
      maximumLength = 450;
      if( currentLength > maximumLength ) {
          $('#postasblog').prop('disabled', true);
          $('#postaswhisper').prop('disabled', true);
      } else if (currentLength <= maximumLength) {
            $('#postasblog').prop('disabled', false);
          $('#postaswhisper').prop('disabled', false);
      }
      console.log(currentLength);
            // Update the counter with text length of editor HTML output.
            // console.log({ value : evt.editor.getData() },this.form.fillpara, 200);
            //    textCounter2( { value : evt.editor.getData() },this.form.grLenght2, 200 );
            } );
            // var data = CKEDITOR.instances.description.getData();
            // console.log(data.length)
        });
    </script>
@endsection