@extends('layouts.newlayout')
@include('inc.function')
@php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; @endphp
@section('meta')
<!-- Facebook and Twitter integration -->
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:image" content="http://blogpara.in/storage/posts/{{$post->picture}}" />
<meta property="og:url" content="{{$actual_link}}" />
<meta property="og:site_name" content="BlogPara" />
<meta property="og:description" content="{!!$post->title!!}" />
<meta name="twitter:title" content="{{$post->title}}" />
<meta name="twitter:image" content="http://blogpara.in/storage/posts/{{$post->picture}}" />
<meta name="twitter:url" content="{{$actual_link}}" />
@php $parent_post = $post->id @endphp
@php $today=date("Y/m/d") @endphp
@endsection

<style>
    a.badge.badge-danger {
        background: #E91E63;
        padding: 0px 7px;
        color: #fff;
        border-radius: 15px;
    }
.card{
    box-shadow: 0 0 13px 0 rgba(236,236,241,.44);
    background: #fff;
    width: 100%;
    padding: 10px 30px;

}
.col-sm-12.view-comment-btn {
    margin-top: -28px;
}
span.bubble {
    background: #E91E63;
    padding: 0px 5px;
    border-radius: 50%;
    color: #fff;
}
.view-comment-btn a {
    color: #007bff;
}
.comment.view-comment-btn{
    /* margin-top: -28px; */
    color: #007bff;
}
.modal-open{
    overflow:hidden
}
.modal-open .modal{
    overflow-x:hidden;
    overflow-y:auto
}
.modal{
    position:fixed;
    top:0;
    right:0;
    bottom:0;
    left:0;
    z-index:1050;
    display:none;
    overflow:hidden;
    outline:0
}
.modal-dialog{
    position:relative;
    width:auto;
    margin:.5rem;
    pointer-events:none
}
.modal.fade .modal-dialog{
    transition:-webkit-transform .3s ease-out;
    transition:transform .3s ease-out;
    transition:transform .3s ease-out,-webkit-transform .3s ease-out;
    -webkit-transform:translate(0,-25%);
    transform:translate(0,-25%)
}
@media screen and (prefers-reduced-motion:reduce){
    .modal.fade .modal-dialog{
        transition:none
    }
}
.modal.show .modal-dialog{
    -webkit-transform:translate(0,0);
    transform:translate(0,0)
}
.modal-dialog-centered{
    display:-ms-flexbox;
    display:flex;
    -ms-flex-align:center;
    align-items:center;
    min-height:calc(100% - (.5rem * 2))
}
.modal-dialog-centered::before{
    display:block;
    height:calc(100vh - (.5rem * 2));
    content:""
}
.modal-content{
    position:relative;
    display:-ms-flexbox;
    display:flex;
    -ms-flex-direction:column;
    flex-direction:column;
    width:100%;
    pointer-events:auto;
    background-color:#fff;
    background-clip:padding-box;
    border:1px solid rgba(0,0,0,.2);
    border-radius:.3rem;
    outline:0
}
.modal-backdrop{
    position:fixed;
    top:0;
    right:0;
    bottom:0;
    left:0;
    z-index:1040;
    background-color:#000
}
.modal-backdrop.fade{
    opacity:0
}
.modal-backdrop.show{
    opacity:.5
}
.modal-header{
    display:-ms-flexbox;
    display:flex;
    -ms-flex-align:start;
    align-items:flex-start;
    -ms-flex-pack:justify;
    justify-content:space-between;
    padding:1rem;
    border-bottom:1px solid #e9ecef;
    border-top-left-radius:.3rem;
    border-top-right-radius:.3rem
}
.modal-header .close{
    padding:1rem;
    margin:-1rem -1rem -1rem auto
}
.modal-title{
    margin-bottom:0;
    line-height:1.5
}
.modal-body{
    position:relative;
    -ms-flex:1 1 auto;
    flex:1 1 auto;
    padding:1rem
}
.modal-footer{
    display:-ms-flexbox;
    display:flex;
    -ms-flex-align:center;
    align-items:center;
    -ms-flex-pack:end;
    justify-content:flex-end;
    padding:1rem;
    border-top:1px solid #e9ecef
}
.modal-footer>:not(:first-child){
    margin-left:.25rem
}
.modal-footer>:not(:last-child){
    margin-right:.25rem
}
.modal-scrollbar-measure{
    position:absolute;
    top:-9999px;
    width:50px;
    height:50px;
    overflow:scroll
}
@media (min-width:576px){
    .modal-dialog{
        max-width:500px;
        margin:1.75rem auto
    }
    .modal-dialog-centered{
        min-height:calc(100% - (1.75rem * 2))
    }
    .modal-dialog-centered::before{
        height:calc(100vh - (1.75rem * 2))
    }
    .modal-sm{
        max-width:300px
    }
}
@media (min-width:992px){
    .modal-lg{
        max-width:800px
    }
}
button.close{
    padding:0;
    background-color:transparent;
    border:0;
    -webkit-appearance:none
}
</style>

@section('main')
<div class="blog_breadcrumb_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="blog_breadcrumb_div">
                    <h3>{{$post->title}}</h3>
                    <ol class="breadcrumb">
                        <li>You are here:</li>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="#">Blog</a></li>
                        <li class="active">{{$post->title}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="blog_main_wrapper blog_toppadder60 blog_bottompadder60">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12 col-12">

                {{-- <form action="/search" method="get">
                    <div class="input-group md-form form-sm form-2 pl-0">
                        <input class="form-control my-0 py-1 amber-border" type="search" name="search" placeholder="Search"
                            aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-danger" id="basic-text1"><i class="fa fa-search text-grey"
                                    aria-hidden="true"></i></button>
                        </div>
                    </div>
                </form> --}}



                <div class="blog_post_style2 blog_single_div">
                    <div class="blog_post_style2_img wow fadeInUp">
                        <img src="/storage/posts/{{$post->picture}}" style="height:500px; width:870px" class="img-fluid"
                            alt="">
                    </div>
                    <div class="blog_post_style2_content wow fadeInUp">
                        <h3>{{$post->title}}</h3>
                        @php $postuser=App\User::find($post->user_id) @endphp
                        <div class="blog_author_data"><a href="/author/{{$postuser->id}}"><img
                                    src="/storage/profile/{{$postuser->profile_picture}}" class="img-fluid" alt="" width="34"
                                    height="34"> {{$postuser->name}}</a></div>
                        <ul class="blog_meta_tags">
                            <li><span class="blog_bg_blue"> <svg xmlns="http://www.w3.org/2000/svg" width="13px"
                                        height="10px" version="1.1" id="Capa_1"
                                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                                        xml:space="preserve">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)" d="M469.51,317c7.14-7.97,11.49-18.49,11.49-30c0-24.81-20.19-45-45-45h-87.34c8.65-26.25,12.34-61.08,12.34-76.01V151
                                c0-33.08-26.92-60-60-60h-15c-6.88,0-12.88,4.68-14.55,11.36l-8.17,32.69c-11.45,45.78-47.8,96.29-85.42,105.47
                                C171.27,223.84,155,212,136,212H46c-8.28,0-15,6.72-15,15v270c0,8.28,6.72,15,15,15h90c17.89,0,33.37-10.49,40.62-25.65
                                l51.54,17.18c16.85,5.62,34.41,8.47,52.18,8.47H406c24.81,0,45-20.19,45-45c0-5.85-1.12-11.45-3.16-16.58
                                C466.92,445.21,481,427.72,481,407c0-11.51-4.35-22.03-11.49-30c7.14-7.97,11.49-18.49,11.49-30S476.65,324.97,469.51,317z
                                 M151,467c0,8.27-6.73,15-15,15H61V242h75c8.27,0,15,6.73,15,15V467z M406,332h30c8.27,0,15,6.73,15,15c0,8.27-6.73,15-15,15h-30
                                c-8.28,0-15,6.72-15,15c0,8.28,6.72,15,15,15h30c8.27,0,15,6.73,15,15c0,8.27-6.73,15-15,15h-30c-8.28,0-15,6.72-15,15
                                c0,8.28,6.72,15,15,15c8.27,0,15,6.73,15,15c0,8.27-6.73,15-15,15H280.34c-14.54,0-28.91-2.33-42.7-6.93L181,456.19V270.58
                                c23.53-4.47,46.56-19.37,67.35-43.76c20.3-23.82,36.76-55.4,44.03-84.49l5.33-21.33H301c16.54,0,30,13.46,30,30v14.99
                                c0,20.14-6.3,58.77-14.36,76.01H286c-8.28,0-15,6.72-15,15c0,8.28,6.72,15,15,15h150c8.27,0,15,6.73,15,15c0,8.27-6.73,15-15,15
                                h-30c-8.28,0-15,6.72-15,15C391,325.28,397.72,332,406,332z" /> </svg>
                                    {{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}</span>
                            </li>
                            <li><span class="blog_bg_pink"> <svg xmlns="http://www.w3.org/2000/svg" width="13px"
                                        height="10px">
                                        <path fill-rule="evenodd" fill="rgb(255, 255, 255)"
                                            d="M12.485,7.049 C12.142,7.544 11.670,7.962 11.070,8.303 C11.119,8.417 11.168,8.520 11.219,8.615 C11.270,8.710 11.330,8.801 11.401,8.889 C11.471,8.977 11.525,9.045 11.564,9.095 C11.603,9.145 11.665,9.214 11.752,9.305 C11.840,9.394 11.895,9.453 11.919,9.482 C11.924,9.487 11.934,9.497 11.948,9.514 C11.963,9.530 11.974,9.542 11.981,9.549 C11.988,9.556 11.998,9.568 12.010,9.585 C12.022,9.602 12.030,9.614 12.035,9.624 L12.053,9.659 C12.053,9.659 12.058,9.673 12.068,9.702 C12.077,9.730 12.078,9.745 12.071,9.748 C12.064,9.750 12.062,9.766 12.064,9.794 C12.050,9.860 12.018,9.912 11.970,9.950 C11.921,9.988 11.868,10.005 11.810,10.000 C11.568,9.967 11.360,9.929 11.186,9.887 C10.441,9.697 9.769,9.394 9.169,8.977 C8.734,9.053 8.309,9.091 7.893,9.091 C6.582,9.091 5.441,8.778 4.469,8.153 C4.749,8.172 4.962,8.182 5.107,8.182 C5.886,8.182 6.633,8.075 7.349,7.862 C8.064,7.649 8.703,7.343 9.264,6.946 C9.868,6.510 10.333,6.008 10.657,5.440 C10.981,4.872 11.143,4.271 11.143,3.637 C11.143,3.272 11.087,2.912 10.976,2.557 C11.600,2.893 12.093,3.315 12.456,3.821 C12.818,4.328 13.000,4.872 13.000,5.455 C13.000,6.023 12.828,6.554 12.485,7.049 ZM7.672,6.787 C6.886,7.111 6.031,7.273 5.107,7.272 C4.691,7.272 4.266,7.235 3.830,7.159 C3.231,7.575 2.558,7.879 1.814,8.068 C1.640,8.111 1.432,8.148 1.190,8.182 L1.168,8.182 C1.115,8.182 1.065,8.163 1.019,8.125 C0.973,8.087 0.946,8.037 0.936,7.976 C0.931,7.962 0.929,7.946 0.929,7.930 C0.929,7.914 0.930,7.898 0.932,7.884 C0.935,7.869 0.939,7.855 0.947,7.841 L0.965,7.805 C0.965,7.805 0.973,7.792 0.990,7.767 C1.007,7.740 1.017,7.729 1.019,7.731 C1.022,7.734 1.033,7.722 1.052,7.696 C1.071,7.670 1.081,7.659 1.081,7.664 C1.105,7.636 1.161,7.577 1.248,7.486 C1.335,7.396 1.398,7.326 1.436,7.277 C1.475,7.227 1.530,7.158 1.600,7.071 C1.670,6.983 1.730,6.892 1.781,6.797 C1.832,6.703 1.881,6.598 1.930,6.485 C1.330,6.144 0.859,5.725 0.515,5.228 C0.172,4.731 0.000,4.200 0.000,3.637 C0.000,2.978 0.227,2.370 0.682,1.812 C1.137,1.253 1.757,0.812 2.543,0.487 C3.329,0.163 4.183,0.000 5.107,0.000 C6.031,0.000 6.886,0.162 7.672,0.487 C8.458,0.812 9.078,1.253 9.532,1.812 C9.987,2.370 10.214,2.978 10.214,3.637 C10.214,4.295 9.987,4.903 9.532,5.462 C9.078,6.020 8.458,6.462 7.672,6.787 ZM8.716,2.280 C8.337,1.859 7.825,1.525 7.182,1.279 C6.539,1.033 5.847,0.910 5.107,0.910 C4.367,0.910 3.676,1.033 3.032,1.279 C2.389,1.525 1.878,1.859 1.498,2.280 C1.119,2.702 0.929,3.154 0.929,3.637 C0.929,4.025 1.057,4.399 1.313,4.759 C1.569,5.119 1.930,5.431 2.394,5.697 L3.098,6.094 L2.844,6.691 C3.008,6.596 3.158,6.503 3.294,6.414 L3.613,6.194 L3.997,6.264 C4.375,6.331 4.745,6.364 5.107,6.364 C5.847,6.364 6.539,6.240 7.182,5.994 C7.825,5.748 8.337,5.415 8.716,4.993 C9.096,4.572 9.286,4.120 9.286,3.637 C9.286,3.154 9.096,2.702 8.716,2.280 Z" />
                                        </svg> {{count($comment=App\Comments::where('post_id',$post->id)->get())}}
                                </span></li>
                        </ul>
                        {{$timeago=get_timeago(strtotime($post->created_at))}}
                        <div class="float-right">
                            @auth
                            <button class="btn btn-danger pull-right likebtn" data-id="{{$post->id}}"><span class="fa fa-thumbs-up"></span>
                                <span id="like_status">
                                    @php $likes=App\Likes::where('user_id',Auth::user()->id)->where('post_id',$post->id)->get()@endphp
                                    @if (count($likes)>0)
                                    @foreach ($likes as $like) @endforeach @php
                                    if($like->status==0){echo "Like";}else{echo "Liked";}
                                    @endphp
                                    @else
                                    Like
                                    @endif
                                </span></button>   
                            @endauth
                        </div>   
                        <p>{!!$post->description!!}</p>

                        {{-- <hr>
                        @php
                        $results= explode(",",$post->tags);
                        @endphp
                        @foreach ($results as $result)
                        <a href="/filter-by-tags/{{$result}}" class="badge badge-danger"><span>{{$result}} </span></a>
                        @endforeach --}}
                        {{-- <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                @auth


                                @if (Auth::user()->id==$post->user_id)


                                @if ($post->whisper_status=="active")

                                @if ($post->invitation==null)
                                <button type="button" class="btn btn-primary " data-toggle="modal"
                                    data-target="#invite">
                                    Invite
                                </button>

                                @elseif($post->expiry_date < $today ) <button type="button" class="btn btn-primary "
                                    data-toggle="modal" data-target="#invite">
                                    Invite change
                                    </button>
                                    @endif
                                    @endif
                                    @endif
                                    @endauth
                            </div>
                            <div class="col-sm-9">
                                <a href="https://facebook.com/sharer.php?display=page&u={{$actual_link}}"
                                    target="_blank" type="button" class="btn "><span class="fa fa-facebook"></span></a>
                                <a href="http://twitter.com/share?text={{$post->title}}&url={{$actual_link}}&hashtags={{$post->tags}}"
                                    target="_blank" type="button" class="btn"><span class="fa fa-twitter"></span></a>
                                <a href="https://www.linkedin.com/sharing/share-offsite/?url={{$actual_link}}"
                                    type="button" target="_blank" class="btn"><span class="fa fa-linkedin"></span></a>
                                <a href="https://in.pinterest.com/pin/create/button/?url={{$actual_link}}" type="button"
                                    target="_blank" class="btn"><span class="fa fa-pinterest"></span></a>
                                <a href="https://wa.me/?text={{$post->title}} {{ urlencode($actual_link) }}" type="button"
                                    target="_blank" class="btn"><span class="fa fa-whatsapp"></span></a>

                            </div>
                        </div> --}}
                    </div>
                </div>





                @if(count($trails=App\Posts::where('post_type','trail')->where('parent_post',$post->id)->get())>0)
                @foreach ($trails as $trail)
                <div class="blog_post_style2 blog_single_div">
                    <div class="blog_post_style2_img wow fadeInUp">
                        <div class="row">
                            <div class="card">
                                <div class="card-body">
                                    <p>  {!!$trail->description!!} </p>
                                        <div class="pull-right">
                                        @php $postuser=App\User::find($trail->user_id); @endphp

                                        <i class="mt-2 mb-2"> by <a href="/author/{{$postuser->id}}">  {{$postuser->name}}</a></i>

                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                     
                  
                      @auth
                      <div class="clearfix">
                      <div class="row">
                          <div class="col-sm-12 mt-3" >
                        @php $today=date("Y/m/d") @endphp
                            @if ($trail->user_id ==Auth::user()->id)
                            @if (count($trailcomment=App\Trail::where('parent_post',$post->id)->where('trail_post',$trail->id)->get())>0)
                                
                            <div class="col-sm-12 view-comment-btn">
                                <a href="/view-comments/{{$post->id}}/{{$trail->id}}">View Comments <span class="bubble">{{count($trailcomment)}}</span></a>
                            </div>
                            @endif
                                @if ($trail->whisper_status=="active") 

                                            @if ($trail->invitation==null)
                                            <button type="button" class="btn btn-primary " style="margin-top:30px" data-post-id=" {{ $trail_id =$trail->id }}" data-toggle="modal" data-target="#invite">
                                            Invite
                                        </button>
                                
                                        @elseif($trail->expiry_date < $today )
                                        <button type="button" class="btn btn-primary "  style="margin-top:30px" data-post-id=" {{ $trail_id=$trail->id }}" data-toggle="modal" data-target="#invite">
                                            Invite change
                                        </button>
                                        @endif
                                @endif
                            @endif
                            @if ($post->user_id==Auth::user()->id)
                            <div class="col-sm-12">
                                    <a href="/comment-trail/{{$post->id}}/{{$trail->id}}" class="comment view-comment-btn">Comment to This Trail</a>
                                </div>
                            @endif
                            </div>
                     </div>
                    </div>
                      @endauth
                      @php $trigger_postid=$trail->id @endphp
                @endforeach
            @else
            @php $trigger_postid=$post->id @endphp

            <div class="blog_post_style2 blog_single_div">
                <div class="blog_post_style2_img wow fadeInUp">
                    <div class="row">
                        <div class="card">
                            <div class="card-body">
                                <p>  No Trails Found </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="blog_post_style2 blog_single_div">
                <div class="blog_post_style2_img wow fadeInUp">
                    <div class="row">
                        <div class="card">
                            <div class="card-body">
                                @php
                                $results= explode(",",$post->tags);
                                @endphp
                                @foreach ($results as $result)
                                <a href="/filter-by-tags/{{$result}}" class="badge badge-danger"><span>{{$result}} </span></a>
                                @endforeach
                                <hr>
            <div class="row">
                <div class="col-sm-3">
                    @auth


                    @if (Auth::user()->id==$post->user_id)


                    @if ($post->whisper_status=="active")

                    @if ($post->invitation==null)
                    <button type="button" class="btn btn-primary " data-toggle="modal"
                        data-target="#invite">
                        Invite
                    </button>

                    @elseif($post->expiry_date < $today ) <button type="button" class="btn btn-primary "
                        data-toggle="modal" data-target="#invite">
                        Invite change
                        </button>
                        @endif
                        @endif
                        @endif
                        @endauth
                </div>
                <div class="col-sm-6 text-center">
                    <a href="https://facebook.com/sharer.php?display=page&u={{$actual_link}}"
                        target="_blank" type="button" class="btn "><span class="fa fa-facebook"></span></a>
                    <a href="http://twitter.com/share?text={{$post->title}}&url={{$actual_link}}&hashtags={{$post->tags}}"
                        target="_blank" type="button" class="btn"><span class="fa fa-twitter"></span></a>
                    <a href="https://www.linkedin.com/sharing/share-offsite/?url={{$actual_link}}"
                        type="button" target="_blank" class="btn"><span class="fa fa-linkedin"></span></a>
                    <a href="https://in.pinterest.com/pin/create/button/?url={{$actual_link}}" type="button"
                        target="_blank" class="btn"><span class="fa fa-pinterest"></span></a>
                    <a href="https://wa.me/?text={{$post->title}} {{ urlencode($actual_link) }}" type="button"
                        target="_blank" class="btn"><span class="fa fa-whatsapp"></span></a>

                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
    </div>
</div>
</div>
</div>



<div class="blog_comment_div">
    <hr>
<h3 class="wow fadeInUp">Comments <span>({{count($comments)}})</span></h3>
<ol class="comment-list">
@if (count($comments)>0)
@foreach($comments as $comment)
<li class="wow fadeInUp">
<div class="blog_comment">
<div class="blog_comment_img">
<img src="https://via.placeholder.com/70x70" class="img-fluid" alt="">
</div>
<div class="blog_comment_data">
<h3>@php $user=App\User::findOrFail($comment->user_id); echo $user->name @endphp <span>-  {{$timeago=get_timeago(strtotime($comment->created_at))}}</span></h3>
<p>{{$comment->comment}}</p>
</div>
</div>
</li>
@endforeach

@endif

</ol>
</div>


<div class="blog_comment_formdiv wow fadeInUp">
    <h3>Leave Comments</h3>
    @guest
    <div class="alert alert-warning" role="alert">
        <a href="{{ route('login') }}"><strong>Please Login to Comment</strong></a>
    </div>
    @else

    <form action="" method="post">
        @csrf
        <input type="hidden" name="post_id" value="{{$post->id}}">
        <div class="form-group">
            <label for=""></label>
            @if (session('error'))
            <i>{{session('error')}}</i>
          @endif
            <textarea type="text" name="comment" required id="" class="form-control" placeholder="" aria-describedby="helpId"
                style="resize:none"></textarea>
            <small id="helpId" class="text-muted">Comment as <b> {{Auth::user()->name}} </b> </small>
        </div>
        <div class="blog_row">
            <button  type="submit"  class="blog_btn blog_bg_pink">Post comment</button>
        </div>
    </form>

    <br> 
    <br> 
    {{-- <button class="btn btn-danger pull-right likebtn" data-id="{{$post->id}}"><span class="fa fa-thumbs-up"></span>
        <span id="like_status">
            @php $likes=App\Likes::where('user_id',Auth::user()->id)->where('post_id',$post->id)->get()@endphp
            @if (count($likes)>0)
            @foreach ($likes as $like) @endforeach @php
            if($like->status==0){echo "Like";}else{echo "Liked";}
            @endphp
            @else
            Like
            @endif
        </span></button> --}}
        @if ($save=App\Save::where('user_id',Auth::user()->id)->where('post_id',$post->id)->first())
            @if ($save->status=='saved')
                 <a href="/save/{{$post->id}}" class="btn btn-dark mt-5"><span class="fa fa-save"></span> Saved</a>    
            @else
                 <a href="/save/{{$post->id}}" class="btn btn-outline-dark mt-5"><span class="fa fa-save"></span> Save</a>
            @endif
        @else
             <a href="/save/{{$post->id}}" class="btn btn-outline-dark mt-5"><span class="fa fa-save"></span> Save</a>
        @endif
        <button class="btn btn-danger" id="reportinvoke">Report This Post</button>

    @endguest

</div>


<!-- Modal -->
<div class="modal fade" id="invite" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Invite By Mail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="/invite" method="post">
                <div class="modal-body">
                        @csrf
                <input type="hidden" name="post_id" value="{{$trigger_postid ?? ''}}">
                    <input type="hidden" name="invite_date" value="{{$today=date("Y/m/d")}}">
                    <input type="hidden" name="expiry_date" value="{{$days_ago = date('Y/m/d', strtotime('+5 days', strtotime($today)))}}">
                        <input type="email" placeholder="Please enter your Email here" name="email" class="form-control" id="" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>

        </div>
    </div>
</div>

            </div>
            @include('inc.theme.sidebar')
        </div>
    </div>
</div>


<div class="modal" id="ReportModal"">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Why are you reporting this post ? </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        @auth
        <!-- Modal body -->
        <div class="modal-body text-center">
           <form action="/report" method="post">
            @csrf
                <div class="form-group">
                    <input type="hidden" name="post_id" value="{{$post->id}}">
                  <label for="">Reason</label>
                  <select type="text" name="reason" id="" class="form-control" placeholder="" aria-describedby="helpId">
                      <option value="It's a Spam">It's a Spam</option>
                        <option value="Nudity or sexual activity"> Nudity or sexual activity </option>
                        <option value="Hate speech or symbols">Hate speech or symbols</option>
                        <option value="Violence or dangerous organizations">Violence or dangerous organizations</option>
                        <option value="Sale of illegal or regulated goods">Sale of illegal or regulated goods</option>
                        <option value="Bullying or harassment">Bullying or harassment</option>
                        <option value="Intellectual property violation">Intellectual property violation</option>
                        <option value="Suicide, self-injury or eating disorders">Suicide, self-injury or eating disorders</option>
                        <option value="Scam or fraud">Scam or fraud</option>
                        <option value="False Information">False Information</option>
                        <option value="I just don't like it">I just don't like it</option>
                  </select>
                </div>
                <div class="form-group">
                    <label for="">Description<small>(optional)</small>  </label>
                    <textarea name="reportdesc" class="form-control" style="resize:none" maxlength="150"></textarea>
                </div>
                <input type="submit" value="Report" class="blog_btn blog_bg_pink">
            </form>
        </div>
        @endauth


      </div>
    </div>
  </div>


@endsection
@section('extrascriptss')
    <script>
        $(document).ready(function(){
            $("#reportinvoke").click(function(){
                $("#ReportModal").modal("show");
            });
        });
    </script>
@endsection