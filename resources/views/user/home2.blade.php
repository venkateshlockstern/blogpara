@extends('layouts.user')
@include('inc.function')
@section('content')
@php $bannerad=App\Ads::where('place','banner')->first() @endphp

<div class="container">
    <header>
        <h1>Wake up the writer in you and Expresss your Heart out </h1>
        <div class="text-center">
            <h5>At blogpara you can write more than a Tweet and less than a Blog.</h5>
            <h5>Share it with friends and family </h5>
        </div>
        <div class="row mar_tp">
            <div class="col-12 col-md-2"></div>
            <div class="col-12 col-md-8">
                <form action="/search" method="get">
                    <div class="input-group md-form form-sm form-2 pl-0">
                        <input class="form-control my-0 py-1 amber-border" type="search" name="search"
                            placeholder="Search" aria-label="Search">
                        <div class="input-group-append">
                            <button class="input-group-text amber lighten-3" id="basic-text1"><i
                                    class="fas fa-search text-grey" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </form>
                <div class="col-12 col-md-2"></div>
            </div>
            <div class="col-12 col-md-2"></div>
        </div>
    </header>
</div>

@guest
<!-- Page Content -->
<div class="container mar_tp">
    <h1 class="hd_1 mar_bt">Read a Trending Posts in Blogpara</h1>
    <div class="row owl-carousel">
        @if (count($posts)>0)
        @foreach ($posts as $post)
        @if ($post->post_type=='blog')
        @php $posttype='blog_detail' @endphp
        @else
        @php $posttype='trail_detail' @endphp
        @endif

        <div class="item  col-md-12">
            <div class="card booking-card">

                <div class="view overlay">
                    <a href="/{{$posttype}}/{{$post->id}}"> <img src="storage/posts/{{$post->picture}}"
                            class="img-fluid" alt="Responsive image" style="height:240px"></a>
                    <a href="#!">
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
                <div class="card-body">
                    <p class="mb-2">{{$timeago=get_timeago(strtotime($post->created_at))}} </p>
                    @if($post->post_type=="blog") <p class="mb-2 float-right badge badge-primary type-badge">BLOG </p>
                    @else <p class="mb-2 float-right badge badge-warning type-badge">WHISPER </p>@endif
                    <h4 class="card-title font-weight-bold"><a href="/{{$posttype}}/{{$post->id}}">{{$post->title}}</a>
                    </h4>
                    @php $postuser=App\User::find($post->user_id) @endphp

                    <i class="mt-2 mb-2"> by <a href="/author/{{$postuser->id}}"> {{$postuser->name}}</a></i>

                    <p class="card-text">{!! substr($post->description,0,250)!!} <a
                            href="/{{$posttype}}/{{$post->id}}">more</a> </p>
                    <div>
                        @php
                        $results= explode(",",$post->tags);
                        $i=1;
                        @endphp
                        @foreach ($results as $result)
                        @php $i++ @endphp
                        @if ($i<=4) <a href="/filter-by-tags/{{$result}}"><span class="badge badge-light">{{$result}}
                            </span></a>
                            @endif
                            @endforeach
                    </div>
                    <hr class="my-4">
                    <ul class="list-unstyled list-inline d-flex justify-content-between mb-0">
                        <li class="list-inline-item mr-0">
                            <div class="chip mr-0"><i class="fas fa-heart" aria-hidden="true"></i>
                                {{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}</div>
                        </li>
                        <li class="list-inline-item mr-0">
                            <div class="chip deep-purple white-text mr-0"><i class="fas fa-comments"
                                    aria-hidden="true"></i>
                                {{count($comment=App\Comments::where('post_id',$post->id)->get())}}</div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        @endforeach
        @else
        No Posts Found
        @endif


    </div>
</div>
@else


@if (count($favs=App\Favourites::where('user_id',Auth::user()->id)->get())>0)

<div class="container">
    @foreach ($favs as $fav)
    @php $category=$fav->category @endphp
    <div class="categ_main mar_tp">
        <h4 class="head4 dashed text-capitalize">{{$fav->category}} </h4>
        <div class="row owl-carousel">
            {{-- category wise post --}}
            @php
            $posts=App\Posts::where('category',$fav->category)->where('status','active')->orderBy('id',"desc")->get();
            @endphp
            @if (count($posts)>0)
            @foreach ($posts as $post)
            @if ($post->post_type=='blog')
            @php $posttype='blog_detail' @endphp
            @else
            @php $posttype='trail_detail' @endphp
            @endif
            @php $postuser=App\User::find($post->user_id) @endphp

            <div class="item col-md-12">
                <div class="card booking-card">

                    <div class="view overlay">
                        <a href="/{{$posttype}}/{{$post->id}}"> <img src="storage/posts/{{$post->picture}}"
                                class="img-fluid" alt="Responsive image" style="height:240px"></a>
                        <a href="#!">
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>
                    <div class="card-body">
                        <p class="mb-2">{{$timeago=get_timeago(strtotime($post->created_at))}} </p>
                        @if($post->post_type=="blog") <p class="mb-2 float-right badge badge-primary type-badge">BLOG
                        </p>@else <p class="mb-2 float-right badge badge-warning type-badge">WHISPER </p>@endif
                        <h4 class="card-title font-weight-bold"><a
                                href="/{{$posttype}}/{{$post->id}}">{{$post->title}}</a></h4>
                        @php $postuser=App\User::find($post->user_id) @endphp

                        <i class="mt-2 mb-2"> by <a href="/author/{{$postuser->id}}"> {{$postuser->name}}</a></i>

                        <p class="card-text">{!! substr($post->description,0,250)!!} <a
                                href="/{{$posttype}}/{{$post->id}}">more</a> </p>
                        <div>
                            @php
                            $results= explode(",",$post->tags);
                            $i=1;
                            @endphp
                            @foreach ($results as $result)
                            @php $i++ @endphp
                            @if ($i<=4) <a href="/filter-by-tags/{{$result}}"><span
                                    class="badge badge-light">{{$result}} </span></a>
                                @endif
                                @endforeach
                        </div>
                        <hr class="my-4">
                        <ul class="list-unstyled list-inline d-flex justify-content-between mb-0">
                            <li class="list-inline-item mr-0">
                                <div class="chip mr-0"><i class="fas fa-heart" aria-hidden="true"></i>
                                    {{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}
                                </div>
                            </li>
                            <li class="list-inline-item mr-0">
                                <div class="chip deep-purple white-text mr-0"><i class="fas fa-comments"
                                        aria-hidden="true"></i>
                                    {{count($comment=App\Comments::where('post_id',$post->id)->get())}}</div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
            @endif

            {{-- catgory wise post --}}
        </div>
    </div>

    @endforeach
</div>

@else

<!-- Page Content -->
<div class="container mar_tp">
    <h1 class="hd_1 mar_bt">Read a Trending Posts in Blogpara</h1>
    <div class="row owl-carousel">
        @if (count($posts)>0)
        @foreach ($posts as $post)

        @if ($post->post_type=='blog')
        @php $posttype='blog_detail' @endphp
        @else
        @php $posttype='trail_detail' @endphp
        @endif
        <div class="item col-md-12">
            <div class="card booking-card">

                <div class="view overlay">
                    <a href="/{{$posttype}}/{{$post->id}}"> <img src="storage/posts/{{$post->picture}}"
                            class="img-fluid" alt="Responsive image" style="height:240px"></a>
                    <a href="#!">
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
                <div class="card-body">
                    <p class="mb-2">{{$timeago=get_timeago(strtotime($post->created_at))}} </p>
                    @if($post->post_type=="blog") <p class="mb-2 float-right badge badge-primary type-badge">BLOG </p>
                    @else <p class="mb-2 float-right badge badge-warning type-badge">WHISPER </p>@endif
                    <h4 class="card-title font-weight-bold"><a href="/{{$posttype}}/{{$post->id}}">{{$post->title}}</a>
                    </h4>
                    @php $postuser=App\User::find($post->user_id) @endphp

                    <i class="mt-2 mb-2"> by <a href="/author/{{$postuser->id}}"> {{$postuser->name}}</a></i>

                    <p class="card-text">{!! substr($post->description,0,250)!!} <a
                            href="/{{$posttype}}/{{$post->id}}">more</a> </p>
                    <div>
                        @php
                        $results= explode(",",$post->tags);
                        $i=1;
                        @endphp
                        @foreach ($results as $result)
                        @php $i++ @endphp
                        @if ($i<=4) <a href="/filter-by-tags/{{$result}}"><span class="badge badge-light">{{$result}}
                            </span></a>
                            @endif
                            @endforeach
                    </div>
                    <hr class="my-4">
                    <ul class="list-unstyled list-inline d-flex justify-content-between mb-0">
                        <li class="list-inline-item mr-0">
                            <div class="chip mr-0"><i class="fas fa-heart" aria-hidden="true"></i>
                                {{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}</div>
                        </li>
                        <li class="list-inline-item mr-0">
                            <div class="chip deep-purple white-text mr-0"><i class="fas fa-comments"
                                    aria-hidden="true"></i>
                                {{count($comment=App\Comments::where('post_id',$post->id)->get())}}</div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        @endforeach
        @else
        No Posts Found
        @endif


    </div>
</div>

@endif


@endguest

<div class="container mar_tp">
    <div class="row">
        <div class="col-12 col-md-3">

        </div>
        <div class="col-12 col-md-6">
            {!!$bannerad->adcode!!}
        </div>
        <div class="col-12 col-md-3">

        </div>
    </div>
</div>


<div class="container mar_tp text-center">
    <h1>You can write little more and tweet and post it here</h1>
    @guest
    <a href="/login" class="btn btn-info ">Login to Fill Your Para</a>

    @else
    <a href="/fill-your-para" class="btn btn-info ">Fill Your Para</a>
    @endguest
</div>

<hr>

<div class="container mar_tp">
    <h1>Choose Your Favourite topics to read in your wall</h1>
    <div class="row">
        <div class="col-12 col-md-12 alg_cen">
            <div class="mn_topic mar_tp">
                <ul>
                    @php $categories=App\Categories::all() @endphp
                    @if (count($categories)>0)
                    @foreach ($categories as $category)
                    <li><a class="topic" href="/categories/{{$category->category}}"><i class="fab fa-slack-hash"
                                aria-hidden="true"></i> {{$category->category}}</a></li>
                    @endforeach
                    @endif
                    <li><a class="topic" href="/categories"><i class="fab fa-slack-hash" aria-hidden="true"></i> View
                            All</a></li>

                </ul>

            </div>
        </div>
    </div>
</div>

<div class="container mar_tp1">
    <div class="row">
        <div class="col-sm-12">
            <img src="{{asset('userassets/images/blogparainfog.png')}}" class="img img-responsive" style="width:100%">
        </div>
    </div>
</div>






@endsection

<script>

</script>
