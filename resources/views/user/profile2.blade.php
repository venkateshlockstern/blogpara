@extends('layouts.main')
@include('inc.function')
@section('section')
@php $bannerad=App\Ads::where('place','banner')->first() @endphp 

<div class="col-12 col-md-9">
    <form action="/search" method="get">
        <div class="input-group md-form form-sm form-2 pl-0">
            <input class="form-control my-0 py-1 amber-border" type="search" name="search" placeholder="Search"
                aria-label="Search">
            <div class="input-group-append">
                <button class="input-group-text amber lighten-3" id="basic-text1"><i class="fas fa-search text-grey"
                        aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="categ_main mar_tp mar_bt">
        <h4 class="head4 dashed">Profile </h4>
        <a  class="btn btn-outline-dark " href="/saved-posts"><span class="fa fa-save "> </span>   Saved Posts</a>
        <a  class="btn btn-outline-dark float-right" data-toggle="modal" data-target="#edit-profile"><span class="fa fa-edit "> </span>   Edit Profile</a>
        

        <div class="row">

            <div class="col-12 col-md-6">
                <b>Name </b> : {{$user->name}} <br>
                <b>Username </b> : {{$user->username ?? 'Not Set'}} <br>
                <b>Email </b> : {{$user->email}} <br>
                <b>Bio </b> : {{$user->bio}} <br>
                <b>URL </b> : {{$user->url}} <br>
                <b>Total Posts </b> : {{count(App\Posts::where('user_id',$user->id)->get())}} <br>
                <b>Active Posts </b> : {{count(App\Posts::where('status','active')->where('user_id',$user->id)->get())}}
                <br>

                <a  class="text-primary " data-toggle="modal" data-target="#change-password"><span class="fa fa-refresh "> </span>   Change Password</a>
                <a  class="text-danger " data-toggle="modal" data-target="#del-profile"><span class="fa fa-trash "> </span>   Delete Profile</a>

                <br>
            </div>
            <div class="col-12 col-md-6">
            <img src="/storage/profile/{{$user->profile_picture}}" alt="" class="img img-responsive img-thumbnail float-right" style="height:150px;">
            </div>

        </div>
       
    </div>

    <div class="categ_main mar_tp mar_bt">
      <h4 class="head4 dashed">
        Favourites
      </h4>
      <div class="row">
        <div class=" col-12 col-md-8">
          <table class="table table-condensed">
            @php $favs=App\Favourites::where('user_id',Auth::user()->id)->get(); @endphp
            @if (count($favs)>0)
              @foreach ($favs as $fav)
              <tr>
                <td>{{$fav->category}}</td>
                <td><a href="/deletefav/{{$fav->id}}" class="btn btn-default">  <span class="fa fa-trash"></span> </a></td>
              </tr>
              @endforeach
           @else
                No Favourites Found  <a href="/select_topic">Click here to add new fav</a>
           @endif

          </table>
         

        </div>
        <div class="col-12 col-md-4">
          <a  href="/select_topic" class="text-primary pull-right" ><span class="fa fa-edit "> </span>   Edit Fav</a>
        </div>

      </div>
    </div>


    <div class="categ_main mar_tp">
        <h4 class="head4 dashed">Posts </h4>
        <div class="row">
            @if (count($posts)>0)
            @foreach ($posts as $post)
            @if ($post->post_type=='blog')
            @php $posttype='blog_detail' @endphp
        @else
        @php $posttype='trail_detail' @endphp 
        @endif
            <div class="col-12 col-md-4">
              <div class="card booking-card">
               
                <div class="view overlay">
                    <a href="/{{$posttype}}/{{$post->id}}">  <img src="storage/posts/{{$post->picture}}" class="img-fluid" alt="Responsive image"
                        style="height:240px"></a>
                    <a href="#!">
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>
                <div class="card-body">
                    <p class="mb-2">{{$timeago=get_timeago(strtotime($post->created_at))}} </p>
                    @if($post->post_type=="blog") <p class="mb-2 float-right badge badge-primary type-badge">BLOG </p>@else <p class="mb-2 float-right badge badge-warning type-badge">WHISPER </p>@endif
                     <h4 class="card-title font-weight-bold"><a href="/{{$posttype}}/{{$post->id}}">{{$post->title}}</a></h4>
                    @php $postuser=App\User::find($post->user_id) @endphp

                    <i class="mt-2 mb-2"> by <a href="/author/{{$postuser->id}}">  {{$postuser->name}}</a></i>

                    <p class="card-text">{!! substr($post->description,0,250)!!} <a
                            href="/{{$posttype}}/{{$post->id}}">more</a> </p>
                    <div>
                        @php
                        $results= explode(",",$post->tags);
                        $i=1;
                        @endphp
                        @foreach ($results as $result)
                        @php $i++ @endphp
                        @if ($i<=4)   <a href="/filter-by-tags/{{$result}}"><span class="badge badge-light">{{$result}} </span></a>
                            @endif
                            @endforeach
                    </div>
                    <hr class="my-4">
                    <ul class="list-unstyled list-inline d-flex justify-content-between mb-0">
                        <li class="list-inline-item mr-0">
                            <div class="chip mr-0"><i class="fas fa-heart" aria-hidden="true"></i>
                                {{count(App\Likes::where('post_id',$post->id)->where('like_status',1)->get())}}</div>
                        </li>
                        <li class="list-inline-item mr-0">
                            <div class="chip deep-purple white-text mr-0"><i class="fas fa-comments"
                                    aria-hidden="true"></i>
                                {{count($comment=App\Comments::where('post_id',$post->id)->get())}}</div>
                        </li>

                    </ul>
                </div>
            </div>
            </div>

            @endforeach

            @else
            <p class="text-center">No Posts found</p>
            @endif
            <div class="text-center" style="margin-left: 15%;">
                <div class="ads mar_tp alg_cen mar_bt">
                  {!!$bannerad->adcode!!}   
                </div>
            </div>

        </div>


    </div>





</div>

<!-- The Modal -->
<div class="modal" id="edit-profile">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Profile</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
         <form action="/edit" method="post" enctype="multipart/form-data">
             @csrf
                <div class="form-group">
                  <label for="">Name</label>
                  <input type="text" name="name" value="{{$user->name}}" class="form-control"  required aria-describedby="helpId">
                </div>
                {{-- <div class="form-group">
                  <label for="">Username</label>
                  <input type="text" name="username" value="{{$user->username}}" class="form-control"  required aria-describedby="helpId">
                </div> --}}
                <div class="form-group">
                    <label for="">Email</label>
                <input type="email" name="email"  value="{{$user->email}}" class="disabled form-control" id="">
                </div>
                <div class="form-group">
                  <label for="">Url</label>
                <input type="url" name="url" value="{{$user->url}}" class="form-control" placeholder=""  aria-describedby="helpId">
                </div>
                <div class="form-group">
                  <label for="">Bio</label>
                <textarea type="text"  name="bio" class="form-control"  placeholder="BIO(max 160 char)" aria-describedby="helpId" maxlength="160">{{$user->bio}}</textarea>
                </div>
                
                <div class="form-group">
                  <label for="">Profile Picture</label>
                  <input type="file" name="profile_picture" id="" accept="image/*" class="form-control" placeholder="Title" aria-describedby="helpId">
                </div>

                <input type="submit" value="Submit" class="btn btn-primary  float-right">
         </form>
        </div>
  
      </div>
    </div>
  </div>


  
<!-- The Modal -->
<div class="modal" id="del-profile">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Profile</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
         <form action="/delete_profile" method="post" enctype="multipart/form-data">
             @csrf
           <b>Your Profile, Posts, Likes & Comments will be deleted Permanently ! Are you Sure Wanted to Delete your Account ?</b>
            <div class="form-group">
              <label for="">Password</label>
              <input type="password" name="password" id="" class="form-control" placeholder="" aria-describedby="helpId">
              <small id="helpId" class="text-muted">Enter your password here</small>
            </div>
            <button type="button" class="btn btn-success" data-dismiss="modal">No. Cancel</button>
         <input type="submit" value="Yes...! Delete" class="btn btn-danger  float-right">
         </form>
        </div>
  
      </div>
    </div>
  </div>



<!-- The Modal -->
<div class="modal" id="change-password">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Change Password</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
         <form action="/changepass" method="post" enctype="multipart/form-data">
             @csrf
            <div class="form-group">
              <label for="">Current Password</label>
              <input type="password" name="oldpassword" id="" class="form-control" placeholder="" aria-describedby="helpId">
              <small id="helpId" class="text-muted">Enter your Current Password here</small>
            </div>
            <div class="form-group">
              <label for="">New Password</label>
              <input type="password" name="newpassword" id="" class="form-control" placeholder="" aria-describedby="helpId">
            </div>
         <input type="submit" value="Change" class="btn btn-primary  float-right">
         </form>
        </div>
  
      </div>
    </div>
  </div>



@endsection