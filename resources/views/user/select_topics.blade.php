@extends('layouts.newlayout')
@include('inc.function')
@section('main')
<style>
    a.badge.badge-danger {
        background: #E91E63;
        padding: 0px 7px;
        color: #fff;
        border-radius: 15px;
    }
.card{
    box-shadow: 0 0 13px 0 rgba(236,236,241,.44);
    background: #fff;
    width: 100%;
    padding: 10px 30px;
    margin-top: 10px;

}
.col-sm-12.view-comment-btn {
    margin-top: -28px;
}
span.bubble {
    background: #E91E63;
    padding: 0px 5px;
    border-radius: 50%;
    color: #fff;
}
.view-comment-btn a {
    color: #007bff;
}
.comment.view-comment-btn{
    margin-top: -28px;
    color: #007bff;
}
.comment-section {
    height: 300px;
    overflow: auto;
}
.post-description {
    background: white;
    padding: 10px 20px;
}
.blog_comment_formdiv h3 {
 margin-bottom: 0px;
}
.card form {
    padding: 50px 20px;
}
</style>
<div class="blog_breadcrumb_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="blog_breadcrumb_div">
                    <h3>Select Your Favorite Topics</h3>
                    <ol class="breadcrumb">
                        <li>You are here:</li>
                        <li><a href="/">Home</a></li>
                        <li><a href="#">Blog</a></li>
                        <li class="active">Select Favourite Topics</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blog_main_wrapper blog_toppadder60 blog_bottompadder60">
   <div class="container">
    <div class="row">
        <div class="col-sm-12">
        <div class="text-center">
            <h3>Choose Your interest categories</h3>    
        </div>    
    </div>
    </div>       
</div> 
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <form action="/select_topic" method="post">
                    @csrf
                        @if (count($categories=App\Categories::all()))
                            @foreach ($categories as $category)
                    <button type="button" class="btn btn-outline-dark"> <input  type="checkbox"   name="categories[]" value="{{$category->category}}" id="{{$category->id}}"> <label for="{{$category->id}}">  {{$category->category}} </label> </button>
                            @endforeach
                        @endif
                        <br>
                        <br>
                        <br>
                        <input type="submit" value="Submit" class="btn btn-success">
                        <a href="/" class="btn btn-outline-secondary">Skip </a>
                    </form>
                </div>
            </div>
            {{-- @include('inc.theme.sidebar') --}}
        </div>
    </div>
</div>

@endsection