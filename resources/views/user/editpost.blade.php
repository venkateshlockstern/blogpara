@extends('layouts.newlayout')
@include('inc.function')
@section('main')
<style>
    a.badge.badge-danger {
        background: #E91E63;
        padding: 0px 7px;
        color: #fff;
        border-radius: 15px;
    }
.card{
    box-shadow: 0 0 13px 0 rgba(236,236,241,.44);
    background: #fff;
    width: 100%;
    padding: 10px 30px;
    margin-top: 10px;

}
.col-sm-12.view-comment-btn {
    margin-top: -28px;
}
span.bubble {
    background: #E91E63;
    padding: 0px 5px;
    border-radius: 50%;
    color: #fff;
}
.view-comment-btn a {
    color: #007bff;
}
.comment.view-comment-btn{
    margin-top: -28px;
    color: #007bff;
}
.comment-section {
    height: 300px;
    overflow: auto;
}
.post-description {
    background: white;
    padding: 10px 20px;
}
.blog_comment_formdiv h3 {
 margin-bottom: 0px;
}
.card form {
    padding: 50px 20px;
}
</style>

<div class="blog_main_wrapper blog_toppadder60 blog_bottompadder60">
   
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                   
                <form action="/update/{{$post->id}}" method="post" enctype="multipart/form-data" >
                    <div class="row">
                        <div class="col-sm-12">
                        <h3>Edit Post</h3>
                        </div>
                    </div>
                    <br>
                    <br>
                    @csrf
                       <div class="form-group">
                         <label for="">Title</label>
                            @error('title')
                            <strong class="text-danger">{{ $message }}</strong>
                            @enderror
                         <input type="text" name="title" value="{{$post->title}}"  class="form-control" placeholder="Title" maxlength="60" required aria-describedby="helpId">
                       </div>
                
                       <div class="form-group"> 
                           <label for="">Category</label>
                           @error('category')
                           <strong class="text-danger">{{ $message }}</strong>
                           @enderror
                           <select name="category" class="form-control" id="" required>
                            <option value="{{$post->category}}">{{$post->category}}</option>
                               @php $categories=App\Categories::all(); @endphp
                               @if (count($categories)>0)
                               @foreach ($categories as $category)
                           <option value="{{$category->category}}">{{$category->category}}</option>
                               @endforeach
                               @endif
                           </select>
                       </div>
                       <div class="form-group">
                         <label for="">Tags</label>
                         @error('tags')
                         <strong class="text-danger">{{ $message }}</strong>
                         @enderror
                         <input type="text" name="tags"  value="{{$post->tags}}"  class="form-control" placeholder="" required aria-describedby="helpId">
                         <small id="helpId" class="text-muted">Should be Separated as (,) Comma</small>
                       </div>
          
                       <div class="form-group">
                        {{-- @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        
                     @enderror --}}
                   
                         <label for="">Description</label>
                         @error('description')
                         <strong class="text-danger">{{ $message }}</strong>
                         @enderror
                         <small> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; * You can write upto 450 Characters</small>
                        
          
          <br>
          
          
          
          <textarea type="text"  name="description" class="form-control" required placeholder="" aria-describedby="helpId" maxlength="450"> {!!$post->description!!}</textarea>
                       </div>
                       
                       <div class="form-group">
                         <label for="">Image</label>
                         <input type="file" name="image" id="" accept="image/*" class="form-control" placeholder="Title" aria-describedby="helpId">
                       </div>
          
                       <br>
                       <button type="submit"  class="blog_btn blog_bg_pink" id="">Update</button>
                       {{-- <div class="row">
                           <div class="col-sm-3">
                            <button id="postasblog" class="blog_btn blog_bg_darkblue">Post as Blog</button>
                        </div>
                        <div class="col-sm-6">
                            <button id="postaswhisper" class="blog_btn blog_bg_pink">Start as Whisper</button>
                        </div>
                       </div> --}}
                      
                </form>

            </div>
            </div>
            {{-- @include('inc.theme.sidebar') --}}
        </div>
    </div>
</div>



@endsection

@section('extrascriptss')
    <script>
        $(document).ready(function(){
            $("#submitinvoke").click(function(){
                $("#confirm-modal").modal("show");
            });
            $("#postasblog").click(function(){
               $("#fillpara").attr('action','/create');
               $("#fillpara").submit();
            });
            $("#postaswhisper").click(function(){
                $("#fillpara").attr('action','/add-trail');
               $("#fillpara").submit();
            });
        });
    </script>
@endsection