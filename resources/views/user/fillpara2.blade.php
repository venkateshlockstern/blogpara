@extends('layouts.main')
@include('inc.function')
@section('section')
@php $bannerad=App\Ads::where('place','banner')->first() @endphp 

<div class="col-12 col-md-9">
    <form action="/search" method="get">
        <div class="input-group md-form form-sm form-2 pl-0">
            <input class="form-control my-0 py-1 amber-border" type="search" name="search" placeholder="Search"
                aria-label="Search">
            <div class="input-group-append">
                <button class="input-group-text amber lighten-3" id="basic-text1"><i class="fas fa-search text-grey"
                        aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="categ_main mar_tp mar_bt">
        <h4 class="head4 dashed">Fill Your Para </h4>
        @if (count($errors)>0)
        @foreach ($errors->all() as $error)
        
        @endforeach
        @endif
        <form action="/create" method="post" enctype="multipart/form-data">
          @csrf
             <div class="form-group">
               <label for="">Title</label>
                  @error('title')
                  <strong class="text-danger">{{ $message }}</strong>
                  @enderror
               <input type="text" name="title" id="" class="form-control" placeholder="Title" maxlength="90" required aria-describedby="helpId">
             </div>
             <div class="form-group">
                 <label for="">Category</label>
                 @error('category')
                 <strong class="text-danger">{{ $message }}</strong>
                 @enderror
                 <select name="category" class="form-control" id="" required>
                     <option value="">--------------</option>
                     @php $categories=App\Categories::all(); @endphp
                     @if (count($categories)>0)
                     @foreach ($categories as $category)
                 <option value="{{$category->category}}">{{$category->category}}</option>
                     @endforeach
                     @endif
                 </select>
             </div>
             <div class="form-group">
               <label for="">Tags</label>
               @error('tags')
               <strong class="text-danger">{{ $message }}</strong>
               @enderror
               <input type="text" name="tags" id="" class="form-control" placeholder="" required aria-describedby="helpId">
               <small id="helpId" class="text-muted">Should be Separated as (,) Comma</small>
             </div>

             <div class="form-group">
              {{-- @error('description')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
              
           @enderror --}}
         
               <label for="">Description</label>
               @error('description')
               <strong class="text-danger">{{ $message }}</strong>
               @enderror
              

<br>




               <textarea type="text"  name="description" class="form-control" required placeholder="" aria-describedby="helpId" maxlength="450"></textarea>
             </div>
             
             <div class="form-group">
               <label for="">Image</label>
               <input type="file" name="image" id="" accept="image/*" class="form-control" placeholder="Title" aria-describedby="helpId">
             </div>

             <input type="submit" value="Submit" class="btn btn-primary ">
      </form>
    </div>






</div>



@endsection