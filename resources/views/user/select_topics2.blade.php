@extends('layouts.main')
@include('inc.function')
@section('section')
<div class="col-12 col-md-9">
    <header>
        <h1>Select your Favorite topics and we will show you the posts according that.</h1>
    </header>
    <form action="/select_topic" method="post">
    @csrf
      @if (count($categories=App\Categories::all()))
          @foreach ($categories as $category)
    <button type="button" class="btn btn-outline-dark"> <input  type="checkbox"   name="categories[]" value="{{$category->category}}" id="{{$category->id}}"> <label for="{{$category->id}}">  {{$category->category}} </label> </button>
          @endforeach
      @endif
      <br>
      <br>
      <br>
      <input type="submit" value="Submit" class="btn btn-success">
      <a href="/" class="btn btn-outline-secondary">Skip </a>
    </form>
   
</div>
@endsection