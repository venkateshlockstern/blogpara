@extends('layouts.main')
@include('inc.function')
@section('section')
<div class="col-12 col-md-9">
    <form action="/search" method="get">
        <div class="input-group md-form form-sm form-2 pl-0">
            <input class="form-control my-0 py-1 amber-border" type="search" name="search" placeholder="Search"
                aria-label="Search">
            <div class="input-group-append">
                <button class="input-group-text amber lighten-3" id="basic-text1"><i class="fas fa-search text-grey"
                        aria-hidden="true"></i></button>
            </div>
        </div>
    </form>

    <div class="categ_main mar_tp mar_bt">
        <h4 class="head4 dashed">Edit Post </h4>
        <div class="row">

            <div class="col-12 col-md-12">
               <form action="/update/{{$post->id}}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="card-body">
        <div class="form-group">
            <label for="">Post Title</label>
            @error('title')
            <strong class="text-danger">{{ $message }}</strong>
            @enderror
            <input type="text" name="title" id="" class="form-control" placeholder="" value="{{$post->title}}" aria-describedby="">
        </div>
        <div class="form-group">
            <label for="">Tags</label>
            @error('tags')
            <strong class="text-danger">{{ $message }}</strong>
            @enderror
        <input type="text" name="tags" id="" class="form-control" value="{{$post->tags}}" aria-describedby="helpId">
            <small id="helpId" class="text-muted">Separated using ( , ) </small>
        </div>
        <div class="form-group">
            <label for="">Category</label>
            @error('category')
            <strong class="text-danger">{{ $message }}</strong>
            @enderror
            <select class="form-control" name="category" id="">
            <option value="{{$post->category}}">{{$post->category}}</option>
                @if (count($categories)>0)
                @foreach ($categories as $category)
                <option value="{{$category->category}}" class="text-capitalize"> {{$category->category}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">

            <label for="">Image</label>
            <input type="file" class="form-control" name="image" id="" placeholder="" accept="image/*"
                aria-describedby="fileHelpId">
            <small id="fileHelpId" class="form-text text-muted">Max 2 MiB</small>
        </div>
        <div class="form-group">
            @error('description')
            <strong class="text-danger">{{ $message }}</strong>
            @enderror
            <label for="">Description</label>
            <textarea class="form-control" id="ckeditor" name="description" id="" rows="3">
                {!!$post->description!!}
            </textarea>
        </div>
        <div class="clearfix">
            <button type="submit" class="btn btn-primary float-right">Update Post</button>
        </div>
    </div>
</form>
            </div>
          

        </div>
    </div>


   




</div>
@endsection