<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Posts;
use App\Categories;
use App\Comments;
use App\Likes;
use App\User;

class UserController extends Controller
{
    public function alluser()
    {
         $users=User::all();
         return view('admin.users')->with('users',$users);
    }
    public function user($id)
    {
         $user=User::findOrFail($id);
         $posts=Posts::where('user_id',$id)->get();
         return view('admin.user_detail')->with(['user'=>$user,'posts'=>$posts]);

    }
}

