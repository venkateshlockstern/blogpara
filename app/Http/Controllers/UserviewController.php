<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Posts;
use App\Categories;
use App\Comments;
use App\Likes;
use App\User;
use App\Favourites;
use App\Trail;
use App\Save;
use App\Report;

class UserviewController extends Controller
{
    public function categories()
    {
        $categories=Categories::all();
        return view('user.categories')->with('categories',$categories);

    }

    public function singlecategory($request)
    {
        // $posts=Posts::where('category',$request)
        // ->where('status','active')
        // ->where('post_type','parent_trail')
        
        // ->get();

        $posts = Posts::where('category', $request)
                        ->where('post_type','!=','trail')
                        ->orderBy('id','desc')
                        ->get();
        return view('user.single_category')->with(['category'=>$request,'posts'=>$posts]);
    }
    
    public function singlepost($id)
    {
        if (Auth::user()) {
            $user=Auth::user();
            $user->unreadNotifications->markAsRead(); 
        }
      

        $post=Posts::findOrFail($id);
        if ($post->status=='inactive') {
            return abort(404);
        }
        $posts=Posts::where('category',$post->category)->where('status','active') 
        ->where('post_type','parent_trail')
        ->paginate(3);
        $comments=Comments::where('post_id',$post->id)->OrderBy('id','DESC')->get();
        return view('user.single')->with(['post'=>$post,'posts'=>$posts,'comments'=>$comments]);
    }

    public function commentpost(Request $request,$id)
    {
        if ($request->input('comment')!=null) {
        $comment=new Comments;
        $comment->post_id=$request->input('post_id');
        $comment->comment=$request->input('comment');
        $comment->user_id=Auth::user()->id;
        $comment->save();
        return redirect()->back();
        }
        else{
            return redirect()->back()->with('error','Add some content to  comment');
        }
    }

    public function like($id)
    {
        $user_id=Auth::user()->id;
        $post_id=$id;
        $likes=Likes::where('post_id',$post_id)
                    ->where('user_id',$user_id)
                    ->get();
       if (count($likes)>0) {
           foreach ($likes as $like) {
               
           }
           $id=$like->id;
           $like_status=$like->like_status;
           $like=Likes::find($id);
           if ($like_status==0) {
              $like->like_status=1;
              $like->save();
              return "liked";
           }else{
            $like->like_status=0;
            $like->save();
            return "disliked";

           }

       }else{
           $like= new Likes;
           $like->user_id=$user_id;
           $like->post_id=$post_id;
           $like->like_status=1;
           $like->save();
           return "liked";

       }
    }


    public function profile()
    {
        $user=User::findOrFail(Auth::user()->id);
      

        $posts=Posts::where('post_type','!=','trail')->where('user_id', Auth::user()->id)
      ->orderBy('id','desc')->get();
        return view('user.profile')->with(['user'=>$user,'posts'=>$posts]);
    }

    public function search(Request $request )
    {
        $key=$request->search;
        // SELECT * FROM `posts` WHERE `title` LIKE '%a%' AND `tags` LIKE '%a%'
        $posts = Posts::where('title', 'LIKE', '%' . $key . '%')
        ->where('post_type','!=','trail')
                        ->orWhere('tags','LIKE','%'.$key.'%')
                        ->orWhere('category','LIKE','%'.$key.'%')
                        
                        ->get();

        return view('user.search')->with('posts',$posts);
    }
    public function tags($keyword)
    {
        $key=$keyword;
        // SELECT * FROM `posts` WHERE `title` LIKE '%a%' AND `tags` LIKE '%a%'
        $posts = Posts::where('tags', 'LIKE', '%' . $key . '%')
                        ->where('post_type','!=','trail')
                        ->get();

        return view('user.tags')->with(['posts'=>$posts,'keyword'=>$key]);
    }
    public function editprofile(Request $request)
    {
        $user=User::findOrFail(Auth::user()->id);
                // return $request;
                                    // handle file upload
                if ($request->hasFile('profile_picture')) {
                // Get file name with the extension
                $filenameWithExt=$request->file('profile_picture')->getClientOriginalName();
                // Get just file name

                $filename= pathinfo($filenameWithExt,PATHINFO_FILENAME);

                // Get just extension
                $extension=$request->file('profile_picture')->getClientOriginalExtension();

                // file name to store
                $fileNameToStore=$filename.'_'.time().'.'.$extension;

                // Upload Image

                $path=$request->file('profile_picture')->storeAs('profile/',$fileNameToStore);

                }
                if ($request->has('name')) {
                    $user->name=$request->input('name');
                }
                if ($request->hasFile('profile_picture')) {
                    $user->profile_picture=$fileNameToStore;
                }
                if ($request->has('bio')) {
                    $user->bio=$request->input('bio');
                }
                if ($request->has('url')) {
                    $user->url=$request->input('url');
                }
                if ($request->has('username')) {
                    $user->username=$request->input('username');
                }
                $user->save();
                return redirect()->back()->with('success','Profile Updated');
    }

    public function deleteprofile(Request $request)
    {
         $user=User::findOrFail(Auth::user()->id);
         $password=$request->input('password');
       if(Hash::check($password, $user->password)){
          $posts=Posts::where('post_type','!=','trail')->where('user_id',$user->id)->get();
          foreach ($posts as $post) {
             $post->delete();
          }
          $comments=Comments::where('user_id',$user->id)->get();
          foreach ($comments as $comment) {
            $comment->delete();
         }
          $likes=Likes::where('user_id',$user->id)->get();
          foreach ($likes as $like) {
            $like->delete();
         }
         
          $user->delete();

 Auth::logout();
  return redirect('/login');


       }
       else{
        return redirect()->back()->with('success','Password Not Matched ');

       }

    }

    public function editpost($id)
    {

        $post=Posts::findOrFail($id);
        if (Auth::user()->id!=$post->user_id) {
           return abort(401);
        }
        $categories=Categories::all();
        return view('user.editpost')->with(['post'=>$post ,'categories'=>$categories]);
    }
    public function changepass(Request $request)
    {
        $user=User::findOrFail(Auth::user()->id);
        $oldpassword=$request->input('oldpassword');
        $newpassword=$request->input('newpassword');
        if(Hash::check($oldpassword, $user->password)){
            $user->password=Hash::make($newpassword);
            $user->save();
            return redirect()->back()->with('success','Password Changed ');

        }else{
        return redirect()->back()->with('error','Password Not Matched ');
        }
    }


public function selecttopic()
{
    return view('user.select_topics');
}

public function savetopic(Request $request )
{
    $user_id=Auth::user()->id;
    $categories=$request->input('categories');
    foreach ($categories as $category) {
        if (count(Favourites::where('user_id',$user_id)->where('category',$category)->get())>0) {
        }else{
        $fav=new Favourites;
        $fav->user_id=$user_id;
        $fav->category=$category;
        $fav->save();
        }
    }
    return redirect('/')->with('success','Favourites Added');
}
public function deltopic($id)
{
    $user_id=Auth::user()->id;
$fav=Favourites::where('user_id',$user_id)->where('id',$id)->first();
$fav->delete();
return redirect()->back()->with('success','Fav Deleted');
}

public function authorview($id)
{
  $user=User::findOrFail($id);
   $posts=Posts::where('post_type','!=','trail')->where('user_id',$id)
   ->where('user_id',$id)
   ->orderBy('id','desc')
   ->get();
  return view('user.author')->with(['user'=>$user,'posts'=>$posts]);
}

public function marknotification()
{
    $user=Auth::user();
    $user->unreadNotifications->markAsRead(); 
    return redirect()->back();
}



public function singletrail($id)
{
    if (Auth::user()) {
        $user=Auth::user();
        $user->unreadNotifications->markAsRead(); 
    }
  

    $post=Posts::findOrFail($id);
    
    $comments=Comments::where('post_id',$post->id)->OrderBy('id','DESC')->get();
    $tcomments=Trail::where('parent_post',$post->id)->OrderBy('id','DESC')->get();
    return view('user.single_trail')->with(['post'=>$post,'comments'=>$comments, 'tcomments' => $tcomments]);
}

public function commentTrail($postid,$trailid)
{
    $post=Posts::findOrFail($postid);
    $trail=Posts::findOrFail($trailid);
    $comments=Trail::where('parent_post',$post->id)->where('trail_post',$trail->id)->get();
    if ($post->user_id!=Auth::user()->id) {
        abort(401);
    }
    return view('trail.comment_trail')->with(['post'=>$post,'trail'=>$trail,'comments'=>$comments]);
}
public function commentTrailPost(Request $request,$postid,$trailid)
{
    $post=Posts::findOrFail($postid);
    $trail=Posts::findOrFail($trailid);
    if ($post->user_id!=Auth::user()->id) {
        abort(401);
    }
    if ($request->input('comment')!=null) {
     
    // return $request->comment;
    // return view('trail.comment_trail')->with(['post'=>$post,'trail'=>$trail]);
    $comment=new Trail;
    $comment->parent_post=$post->id;
    $comment->trail_post=$trail->id;
    $comment->parent_user=$post->user_id;
    $comment->trail_user=$trail->user_id;
    $comment->comments=$request->comment;
   
    $comment->save();
    return redirect()->back()->with('success','comment added');
}else{
    return redirect()->back()->with('error','Add some content to  comment');
}
}

public function viewComments($postid,$trailid)
{
    $post=Posts::findOrFail($postid);
    $trail=Posts::findOrFail($trailid);
    if ($trail->user_id!=Auth::user()->id) {
        abort(401);
    }
    $comments=Trail::where('parent_post',$post->id)->where('trail_post',$trail->id)->where('trail_user',Auth::user()->id)->get();
    return view('trail.view_comments')->with(['post'=>$post,'trail'=>$trail,'comments'=>$comments]);
}



public function savepost($post_id)
{
  $user_id=Auth::user()->id;
  $save=Save::where('post_id',$post_id)
                    ->where('user_id',$user_id)
                    ->first();
    if($save){
        if ($save->status=='saved') {
            // $save=new Save;
            $save->user_id=$user_id;
            $save->post_id=$post_id;
            $save->status='unsaved';
            $save->save();
            return  redirect()->back();
        }else{
            // $save=new Save;
            $save->user_id=$user_id;
            $save->post_id=$post_id;
            $save->status='saved';
            $save->save();
            return  redirect()->back();
        }
    }else{
       
            $save=new Save;
            $save->user_id=$user_id;
            $save->post_id=$post_id;
            $save->status='saved';
            $save->save();
            return  redirect()->back();
    }
}
public function savedposts()
{
    $saved=Save::where('user_id',Auth::user()->id)->where('status','saved')->get();
    return view('user.saved_posts')->with('saved',$saved);
}

public function report(Request $request)
{
//    return $request; 
$report=new Report;
$report->post_id=$request->input('post_id');
$report->report_statement=$request->input('reason');
$report->report_description=$request->input('reportdesc');
$report->user_id=Auth::user()->id;
$report->save();
return redirect()->back()->with('success','Your Report Sent Successfully.');
}

}


