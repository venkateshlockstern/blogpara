<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Categories;
use App\Posts;
use App\Ads;

class HomeController extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()) {
            $admin_id=Auth::user()->id;
            if ($admin_id!=1){
                $posts = Posts::orderBy('id', 'DESC')->where('status','active')
                 ->where('post_type','parent_trail')
                 ->orWhere('post_type','blog')
                 ->get();
                return view('user.home')->with('posts',$posts);
            }
            else{
                return view('admin.dashboard');
            }
        }
        else{
            $posts = Posts::orderBy('id', 'DESC')->where('status','active')
            ->where('post_type','parent_trail')
                 ->orWhere('post_type','blog')
            ->get();
             return view('user.home')->with('posts',$posts);
        }
       
    }

    public function showcategories()

    {
        $admin_id=Auth::user()->id;
        if ($admin_id!=1){
            return redirect()->back()->with(['status' => 'Unauthorized Access']); 
        }
        $categories = Categories::all();
        return view('admin.categories')->with('categories',$categories);
    }

      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addcategories(Request $request)
    {
        $admin_id=Auth::user()->id;
        if ($admin_id!=1){
            return redirect()->back()->with(['status' => 'Unauthorized Access']); 
        }
       
        $category= new Categories;
        $category->category=$request->input('category');
        $category->save();
        return redirect('/admincategories')->with('status','Category Successfully Added');

    }

    public function deletecategory($id){
        $admin_id=Auth::user()->id;
        if ($admin_id!=1){
            return redirect()->back()->with(['status' => 'Unauthorized Access']); 
        }
      $category = Categories::findOrFail($id);
      $category->delete();
      return redirect('/admincategories')->with('status','Category Deleted');

    }


    public function ads()
    {
        $ads=Ads::all();
        return view('admin.ads')->with('ads',$ads);
    }
}
