<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Posts;
use App\Categories;
use App\Comments;
use App\Likes;
use App\Ads;
use App\User;
use App\Report;
use App\Notifications\NewPost;
use Illuminate\Support\Facades\Validator;


class PostsController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    
    {
        $admin_id=Auth::user()->id;
        if ($admin_id!=1){
            return redirect()->back()->with(['status' => 'Unauthorized Access']); 
        }
        $posts =Posts::orderBy('id', 'DESC')->paginate(15);
        return view('admin.posts')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admin_id=Auth::user()->id;
        if ($admin_id!=1){
            return redirect()->back()->with(['status' => 'Unauthorized Access']); 
        }
        $categories=Categories::all();
        // return $categories;
       return view('admin.create')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:250',
            'category' => 'required',
            'tags'=>'required',
            'description'=>'required|max:450'
        ]);

        if ($validator->fails()) {
            return redirect('/fill-your-para')
                        ->withErrors($validator)
                        ->withInput();
        }
       
       
        $user_id=Auth::user()->id;
        
        // return $request;
                                    // handle file upload
if ($request->hasFile('image')) {
    // Get file name with the extension
    $filenameWithExt=$request->file('image')->getClientOriginalName();
    // Get just file name

    $filename= pathinfo($filenameWithExt,PATHINFO_FILENAME);

    // Get just extension
    $extension=$request->file('image')->getClientOriginalExtension();

// file name to store
$fileNameToStore=$filename.'_'.time().'.'.$extension;

// Upload Image

 $path=$request->file('image')->storeAs('posts/',$fileNameToStore);



} else {
    $imageSelect = rand(1, 4);

    $fileNameToStore = "df" . $imageSelect . ".jpeg";
}



   
   
        $post= new Posts;
        $post->title=$request->input('title');
        $post->tags=$request->input('tags');
        $post->user_id=$user_id;
        $post->category=$request->input('category');
        $post->description=$request->input('description');
        $post->picture=$fileNameToStore;   
        $users=User::all();
        
        $post->save();
        $lastInsertedId= $post->id;
        foreach ($users as $user) {
            $user->notify(new NewPost($lastInsertedId,"$post->title"));
        }
        return redirect('/')->with('success','Post Created Successfully');
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin_id=Auth::user()->id;
        if ($admin_id!=1){
            return redirect()->back()->with(['status' => 'Unauthorized Access']); 
        }
        $post=Posts::findOrFail($id);
        $comments=Comments::where('post_id',$post->id)->OrderBy('id','DESC')->get();

        // return $comments;
    return view('admin.singlepost')->with(['post'=>$post,'comments'=>$comments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:250',
            'category' => 'required',
            'tags'=>'required',
            'description'=>'required|max:450'
        ]);

        if ($validator->fails()) {
            return redirect('/fill-your-para')
                        ->withErrors($validator)
                        ->withInput();
        }
                                            // handle file upload
if ($request->hasFile('image')) {
    // Get file name with the extension
    $filenameWithExt=$request->file('image')->getClientOriginalName();
    // Get just file name

    $filename= pathinfo($filenameWithExt,PATHINFO_FILENAME);

    // Get just extension
    $extension=$request->file('image')->getClientOriginalExtension();

// file name to store
$fileNameToStore=$filename.'_'.time().'.'.$extension;

// Upload Image

$path=$request->file('image')->storeAs('public/posts/',$fileNameToStore);

}else{
            $imageSelect = rand(1, 4);

            $fileNameToStore = "df" . $imageSelect . ".jpeg";
}

        $post= Posts::findOrFail($id);
        $post->title=$request->input('title');
        $post->tags=$request->input('tags');
        $post->category=$request->input('category');
        $post->description=$request->input('description');
        if ($request->hasFile('image')) {
            $post->picture=$fileNameToStore;
        }
        $post->save();
        return redirect()->back()->with('success','Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $admin_id=Auth::user()->id;
        // if ($admin_id!=1){
        //     return redirect()->back()->with(['status' => 'Unauthorized Access']); 
        // }
        $post=Posts::findOrFail($id);
        $user_id=$post->user_id;
        if ($user_id!=Auth::user()->id) {
            return abort(401);
        }
        else{
            $post->delete();
            return redirect()->back()->with('success','Post Deleted');
        }
       

    }

    public function editpost($id)
    {
        $admin_id=Auth::user()->id;
        if ($admin_id!=1){
            return redirect()->back()->with(['status' => 'Unauthorized Access']); 
        }
        $post=Posts::findOrFail($id);
        $categories=Categories::all();
        return view('admin.edit')->with(['post'=>$post ,'categories'=>$categories]);
    }

    public function approve($id){
        $admin_id=Auth::user()->id;
        if ($admin_id!=1){
            return redirect()->back()->with(['status' => 'Unauthorized Access']); 
        }
        $post=Posts::findOrFail($id);
        $status=$post->status;
        if ($status=='active') {
            $post->status='inactive';
        }else{
            $post->status='active';
        }
        $post->save();
        return redirect('/posts')->with('status','Post Status Changed');
    }
  
    public function banneradchange(Request $request)
    {
        $ad=Ads::where('place','banner')->first();
        $ad->adcode=$request->input('banner');
        $ad->save();
        return redirect()->back()->with('status','ad code changed');
    }
    public function sidebarchange(Request $request)
    {
        $ad=Ads::where('place','sidebar')->first();
        $ad->adcode=$request->input('sidebar');
        $ad->save();
        return redirect()->back()->with('status','ad code changed');
    }



    public function report()
    
    {
        $admin_id=Auth::user()->id;
        if ($admin_id!=1){
            return redirect()->back()->with(['status' => 'Unauthorized Access']); 
        }
        $reports =Report::orderBy('id', 'DESC')->paginate(15);
        return view('admin.report')->with('reports',$reports);
    }
}
