<?php

namespace App\Http\Controllers;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Posts;
use App\Categories;
use App\Comments;
use App\Likes;
use App\User;
use App\Favourites;
use App\Notifications\NewPost;
use App\Notifications\TrailAdded;
use Illuminate\Support\Facades\Validator;


class WhisperController extends Controller
{
    public function addTrail()
    {
        return view('trail.add_trail');
    }
    public function addTrailPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:250',
            'category' => 'required',
            'tags'=>'required',
            'description'=>'required|max:450'
        ]);

        if ($validator->fails()) {
            return redirect('/fill-your-para')
                        ->withErrors($validator)
                        ->withInput();
        }
       

        // return $request;
$user_id=Auth::user()->id;
                                          // handle file upload
if ($request->hasFile('image')) {
    // Get file name with the extension
    $filenameWithExt=$request->file('image')->getClientOriginalName();
    // Get just file name

    $filename= pathinfo($filenameWithExt,PATHINFO_FILENAME);

    // Get just extension
    $extension=$request->file('image')->getClientOriginalExtension();

// file name to store
$fileNameToStore=$filename.'_'.time().'.'.$extension;

// Upload Image

 $path=$request->file('image')->storeAs('posts/',$fileNameToStore);

}else{
            $imageSelect = rand(1, 4);

            $fileNameToStore = "df" . $imageSelect . ".jpeg";
}
$post= new Posts;
$post->title=$request->input('title');
$post->tags=$request->input('tags');
$post->user_id=$user_id;
$post->category=$request->input('category');
$post->description=$request->input('description');
$post->post_type=$request->input('post_type');
$post->thread_status=$request->input('thread_status');
$post->picture=$fileNameToStore;   

$users=User::all();

$post->save();
$lastInsertedId= $post->id;
foreach ($users as $user) {
$user->notify(new NewPost($lastInsertedId,"$post->title"));
}
return redirect('/trail_detail/'.$post->id)->with('success','Trail Created Successfully');

    }




public function myTrails()
{
    $posts=Posts::where('user_id',Auth::user()->id)
    ->where('post_type','parent_trail')
    ->get();
    return view('trail.my_trail')->with('posts',$posts);
}

public function mySingleTrails($id)
{

    $post=Posts::findOrFail($id);
  
    if ($post->user_id!=Auth::user()->id) {
        abort(403);
    }
    if ($post->status=='inactive') {
        return abort(404);
    }
    $posts=Posts::where('category',$post->category)->where('status','active')->paginate(3);
    $comments=Comments::where('post_id',$post->id)->OrderBy('id','DESC')->get();
    

    return view('trail.my_single_trail')->with(['post'=>$post,'posts'=>$posts,'comments'=>$comments]);

}


public function invite(Request $request)
{
    $post=Posts::findOrFail($request->input('post_id'));
   
    if ($post->user_id!=Auth::user()->id) {
        abort(403);
    }
    if ($post->status=='inactive') {
        return abort(404);
    }
    // return $request;
    $post->invitation=$request->input('email');
    $post->invite_date=$request->input('invite_date');
    $post->expiry_date=$request->input('expiry_date');
    $user=User::where('email',$request->input('email'))->first();
    if($user!=null){
            $user->notify(new TrailAdded());

    }else{
        session_start();
        $_SESSION["EMAIL"]=$request->email;
        // $receiver=$request->email;
            $data = array('name'=>"Blog  Para");
            Mail::send('mail', $data, function($message) {
                $receiver=$_SESSION["EMAIL"];
            $message->to($receiver, 'Blogpara')->subject
                ('SomeBody invited to you for a Blogpara Trail please check it out');
            $message->from('support@blogpara.in','Blogpara');
            });
        echo "HTML Email Sent. Check your inbox.";
        }
    $post->save();
    return redirect()->back()->with('success','invited');
}

public function invitesomeone()
{
    // $email=$_GET["email"];
    $data = array('name'=>"Blog  Para");
    Mail::send('invite', $data, function($message) {
    $message->to($_GET["email"], 'Blogpara')->subject('SomeBody invited to you for a Blogpara  please check it out');
    $message->from('support@blogpara.in','Blogpara');
    });
return redirect()->back()->with('success','Invitation Sent Successfully');
}

public function myInvitation()
{
   $posts=Posts::where('invitation',Auth::user()->email)
        ->where('whisper_status','active')
   ->get();
   if (Auth::user()) {
    $user=Auth::user();
    $user->unreadNotifications->markAsRead(); 
}
   return view('trail.view_invitation')->with('posts',$posts);
}

public function participate($id)
{
    $post=Posts::findOrFail($id);
    if ($post->invitation!=Auth::user()->email) {
       abort(403);
    }
    if ($post->expiry_date<date('Y/m/d')) {
        abort(403);
    }
    // if ($post->whisper_status!='active') {
    //     abort(403);
    // }
    return view('trail.participate')->with('post',$post);
   
}



public function postParticipate(Request $request,$id)
{
   $oldpost=Posts::findOrFail($id);
   $oldpost->whisper_status='inactive';
   if ($oldpost->parent_post==null) {
      $parent=$oldpost->id;
   }
   else{
       $parent=$oldpost->parent_post;
   }
   $oldpost->save();
    $post=new Posts;
    $post->title=$oldpost->title;
    $post->category=$oldpost->category;
    $post->parent_post=$parent;
    $post->tags=$oldpost->tags;
    $post->picture=$oldpost->picture;
    $post->user_id=Auth::user()->id;
    $post->post_type="trail";
    $post->description=$request->input('description');
    $post->save();
    $postid = $post['id'];
    return Redirect("/trail_detail/$parent");

}

}
