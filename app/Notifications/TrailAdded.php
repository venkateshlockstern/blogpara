<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class TrailAdded extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $username=Auth::user()->name;
        return (new MailMessage)
        ->line($username.' Believes you are the best person to continue the story - Join in - It is a fabulous new world out there...')
        ->action('Continue '.$username.'\'s Story', url('/invitation'))
        ->line('Here is what you can do,')
        ->line('1. Continue '.$username.'\'s story by Clicking the Button above Invite another buddy who would help with continuing your story.')
        ->line('2. Start your very own story by writing a Blogpara-whisper and Invite a buddy to help you continue your very own story');
    }
    public function toDatabase($notifiable)
    {
        return[
            'postid'=>'invitation',
              'title'=>'Invited '
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
