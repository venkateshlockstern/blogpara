<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTableToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('post_type')->default('blog');
            $table->string('invitation')->nullable();
            $table->string('invite_date')->nullable();
            $table->string('expiry_date')->nullable();
            $table->string('thread_status')->default('active');
            $table->string('whisper_status')->default('active');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
        $table->dropColumn('post_type');
        $table->dropColumn('invitation');
        $table->dropColumn('invite_date');
        $table->dropColumn('expiry_date');
        $table->dropColumn('thread_status');
        $table->dropColumn('whisper_status');
        });
    }
}
