$(document).ready(function(){
        $(".likebtn").click(function(){
           var post_id= $(this).data("id");
           $.ajax(
            {
            url: "/like/"+post_id,
            type: 'get',
            success: function (data){
              if (data=="liked") {
                  $('#like_status').html("Liked");
              }else{
                $('#like_status').html("Like");

              }
            }
            });
});

});


$(document).ready(function(){
//   $(".owl-carousel").owlCarousel({
//     stagePadding: 0,
//     items:3,
//     loop:true,
//     margin:0,
//     autoPlay:true
//     // singleItem:true,
  
 
// });
CKEDITOR.plugins.addExternal( 'wordcount', '/userassets/plugins/wordcount/', 'plugin.js' );
CKEDITOR.plugins.addExternal( 'notification', '/userassets/plugins/notification/', 'plugin.js' );
CKEDITOR.replace('description',{
  toolbarGroups: [{
    "name": "basicstyles",
    "groups": ["basicstyles"]
  },
  {
    "name": "links",
    "groups": ["links"]
  },
  {
    "name": "paragraph",
    "groups": ["list", "blocks"]
  },
  {
    "name": "document",
    "groups": ["mode"]
  },
  {
    "name": "insert",
    "groups": ["insert"]
  },
  {
    "name": "styles",
    "groups": ["styles"]
  },
  {
    "name": "about",
    "groups": ["about"]
  }
],
removeButtons: 'Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Anchor,Language,BidiRtl,BidiLtr,CreateDiv,Find,Replace,Cut,Copy,Paste,PasteText,PasteFromWord,Templates,Save,NewPage,Preview,Print,Source,Formatting Styles,Paragraph Format,Image,SelectAll,Scayt,Subscript,Superscript,RemoveFormat,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,ShowBlocks',
resize_enabled : false,
extraPlugins: 'wordcount,notification',
removePlugins: 'elementspath',

wordcount: {
  onUpdate: stats => {
                // Prints the current content statistics.
                console.log( `Characters: ${ stats.characters }\nWords: ${ stats.words }` );
            },
    // Whether or not you want to show the Paragraphs Count
    showParagraphs: true,

    // Whether or not you want to show the Word Count
    showWordCount: true,

    // Whether or not you want to show the Char Count
    showCharCount: true,

    // Whether or not you want to count Spaces as Chars
    countSpacesAsChars: true,

    // Whether or not to include Html chars in the Char Count
    countHTML: true,
    
  //   // Maximum allowed Word Count, -1 is default for unlimited
  //   maxWordCount: -1,

  //   // Maximum allowed Char Count, -1 is default for unlimited
    maxCharCount: 450,

  //  // Option to limit the characters in the Editor, for example 200 in this case.
   charLimit: 450,
}
});
});

