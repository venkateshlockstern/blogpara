<?php
use App\Posts;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

// whisper

Route::get('/add-trail', 'WhisperController@addTrail')->middleware('auth');
Route::post('/add-trail', 'WhisperController@addTrailPost')->middleware('auth');
Route::get('/my-trails', 'WhisperController@myTrails')->middleware('auth');
Route::get('/my-trails/{id}', 'WhisperController@mySingleTrails')->middleware('auth');
Route::get('/invite', 'WhisperController@invitesomeone');
Route::post('/invite', 'WhisperController@invite');
Route::get('/invitation', 'WhisperController@myInvitation')->middleware('auth');
Route::get('/invited-trail', 'WhisperController@invitedTrail')->middleware('auth');
Route::get('/participate/{id}', 'WhisperController@participate')->middleware('auth');
Route::post('/participate/{id}', 'WhisperController@postParticipate')->middleware('auth');

Route::get('/', 'HomeController@index');
Route::get('/admincategories', 'HomeController@showcategories')->middleware('auth');
Route::get('/delete_category/{id}', 'HomeController@deletecategory')->middleware('auth');
Route::post('/admincategories', 'HomeController@addcategories')->middleware('auth');
Route::get('/ads', 'HomeController@ads')->middleware('auth');

// Posts

Route::get('/posts','PostsController@index')->middleware('auth');
Route::get('/post/{id}','PostsController@show')->middleware('auth');
Route::get('/create','PostsController@create')->middleware('auth');
Route::post('/create','PostsController@store')->middleware('auth');
Route::get('/delete_post/{id}','PostsController@destroy')->middleware('auth');
Route::get('/edit_post/{id}','PostsController@editpost')->middleware('auth');
Route::post('/update/{id}','PostsController@update')->middleware('auth');
Route::get('/approve_post/{id}','PostsController@approve')->middleware('auth');
Route::post('/bannerad','PostsController@banneradchange')->middleware('auth');
Route::post('/sidebar','PostsController@sidebarchange')->middleware('auth');


// Users
Route::get('/users', 'UserController@alluser');
Route::get('/user/{id}', 'UserController@user');

// User View

Route::get('/categories','UserviewController@categories');
Route::get('/categories/{category}','UserviewController@singlecategory');
Route::get('/blog-detail/{id}','UserviewController@singlepost');
Route::get('/blog_detail/{id}','UserviewController@singlepost');
Route::get('/filter-by-tags/{keyword}','UserviewController@tags');
Route::post('/blog-detail/{id}','UserviewController@commentpost');
Route::post('/blog_detail/{id}','UserviewController@commentpost');
Route::post('/trail_detail/{id}','UserviewController@commentpost');
Route::get('/like/{id}','UserviewController@like');
Route::get('/profile','UserviewController@profile')->middleware('auth');
Route::get('/search','UserviewController@search');
Route::post('/edit','UserviewController@editprofile')->middleware('auth');
Route::post('/delete_profile','UserviewController@deleteprofile')->middleware('auth');
Route::get('/editpost/{id}','UserviewController@editpost')->middleware('auth');
Route::post('/changepass','UserviewController@changepass')->middleware('auth');
Route::get('/select_topic','UserviewController@selecttopic')->middleware('auth');
Route::post('/select_topic','UserviewController@savetopic')->middleware('auth');
Route::get('/deletefav/{id}','UserviewController@deltopic')->middleware('auth');
Route::get('/author/{id}','UserviewController@authorview');
Route::get('/marknotificationasread','UserviewController@marknotification')->middleware('auth');
Route::get('/fill-your-para',function(){
    return view('user.fillpara');
})->middleware('auth');



// user trail
Route::get('/trail_detail/{id}','UserviewController@singletrail');
Route::get('/comment-trail/{postid}/{trailid}','UserviewController@commentTrail');
Route::post('/comment-trail/{postid}/{trailid}','UserviewController@commentTrailPost');
Route::get('/view-comments/{postid}/{trailid}','UserviewController@viewComments');
Route::get('/blog-detail/invitation',function ()
{
    return redirect('/invitation');
});


Route::get('/save/{post_id}','UserviewController@savepost')->middleware('auth');
Route::get('/saved-posts','UserviewController@savedposts')->middleware('auth');


Route::get('/single',function(){return view('user.singlepage');});
// Route::get('/invite',function(){return view('invite');});

Route::post('/report','UserviewController@report');
Route::get('/reports','PostsController@report')->middleware('auth');

Route::get('/oldhome',function(){
    $posts=Posts::all();
    return view('user.home2')->with('posts',$posts);
});